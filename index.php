<!DOCTYPE html>
<html lang="en">
    <head>
        <title>MarieTeam</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="Destino project">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
        <link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
        <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
        <link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="styles/main_styles.css">
        <link rel="stylesheet" type="text/css" href="styles/responsive.css">
    </head>
    <body>

        <div class="super_container">

            <?php include("php/navbar.inc.php");?>

            <!-- Home -->

            <div class="home">
                <div class="home_background" style="background-image:url(images/home.jpg)"></div>
                <div class="home_content">
                    <div class="home_content_inner">
                        <div class="home_text_large">Voyage</div>
                        <div class="home_text_small">Partir plus loin </div>
                    </div>
                </div>
            </div>

            <!-- Find Form -->

            <div class="find">
                <!-- Image by https://unsplash.com/@garciasaldana_ -->
                <div class="find_background parallax-window" data-parallax="scroll" data-image-src="images/find.jpg" data-speed="0.8"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="find_title text-center">Trouvez votre voyage</div>
                        </div>
                        <div class="col-12">
                            
                            <div class="find_form_container">
                               
                                <form action="Choix_horaire.php" method="post" id="find_form" class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap">
                                    <div class="find_item">
                                        <div>Liaisons :</div>
                                        <select name="liaison" id="adventure" class="dropdown_item_select find_input">
                                            <!-- form_find -->
                                            <?php include"php/formFind.inc.php";
                                            entreeLiaison();
                                            ?>
                                        </select>
                                    </div>
                                    <div class="find_item">
                                        <div>Date :</div>
                                        <input type="date" name="date" id="adventure" class="dropdown_item_select find_input">
                                    </div>

                                    <button type="submit" class="button find_button">Envoyer</button>
                                </form>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>


            <!-- Top Destinations -->

            <div class="top">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="section_title text-center">
                                <h2>Top destinations</h2>
                                <div>Les meilleures destinations pour vous</div>
                            </div>
                        </div>
                    </div>
                    <div class="row top_content">

                        <div class="col-lg-3 col-md-6 top_col">

                            <!-- Top Destination Item -->
                            <div class="top_item">
                                <a href="offers.php">
                                    <div class="top_item_image"><img src="images/top_1.jpg" alt="https://unsplash.com/@sgabriel"></div>
                                    <div class="top_item_content">
                                        <div class="top_item_text">Aix en Provence</div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 top_col">

                            <!-- Top Destination Item -->
                            <div class="top_item">
                                <a href="offers.php">
                                    <div class="top_item_image"><img src="images/top_2.jpg" alt="https://unsplash.com/@jenspeter"></div>
                                    <div class="top_item_content">
                                        <div class="top_item_text">Belle-île-en-mer</div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 top_col">

                            <!-- Top Destination Item -->
                            <div class="top_item">
                                <a href="offers.php">
                                    <div class="top_item_image"><img src="images/top_3.jpg" alt="https://unsplash.com/@anikindimitry"></div>
                                    <div class="top_item_content">
                                        <div class="top_item_text">Ile de Groix</div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 top_col">

                            <!-- Top Destination Item -->
                            <div class="top_item">
                                <a href="offers.php">
                                    <div class="top_item_image"><img src="images/top_4.jpg" alt="https://unsplash.com/@jenspeter"></div>
                                    <div class="top_item_content">
                                        <div class="top_item_text">Batz</div>
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Reduction -->

            <div class="last">
                <!-- Image by https://unsplash.com/@thanni -->
                <div class="last_background parallax-window" data-parallax="scroll" data-image-src="images/last.jpg" data-speed="0.8"></div>

                <div class="container">
                    <div class="row">
                        <div class="last_logo"><img src="images/last_logo.png" alt=""></div>
                        <div class="col-lg-12 last_col">
                            <div class="last_item">
                                <div class="last_item_content">
                                    <div class="last_percent">25%</div>
                                    <div class="last_title">Pour voyager malin</div>
                                    <div class="last_text">Pour toute reservation de deux mois à l'avance vous obtenez 25 points MarieTeam, au bout de 100 points vous obtenez 25% sur votre prochain voyage. Tous vos points sont cumulés sur votre adresse mail.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Special -->

            <!--CARROUSSEL-->
            <div class="special">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="section_title text-center">
                                <h2>Toutes nos offres</h2>
                                <div>Des îles d'exception</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="special_content">
                    <div class="special_slider_container">
                        <div class="owl-carousel owl-theme special_slider">

                            <!-- Special Offers Item -->
                            <div class="owl-item">
                                <div class="special_item d-flex flex-column align-items-center justify-content-center">
                                    <div class="special_item_background"><img src="images/special_1.jpg" alt="https://unsplash.com/@varshesh"></div>
                                    <div class="special_item_content text-center">
                                        <div class="special_category">Bain de soleil</div>
                                        <div class="special_title"><a href="offers.php">Belle-île-en-mer</a></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Special Offers Item -->
                            <div class="owl-item">
                                <div class="special_item d-flex flex-column align-items-center justify-content-center">
                                    <div class="special_item_background"><img src="images/special_2.jpg" alt="https://unsplash.com/@paulgilmore_"></div>
                                    <div class="special_item_content text-center">
                                        <div class="special_category">Sunbathing</div>
                                        <div class="special_title"><a href="offers.php">Aix en Provence</a></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Special Offers Item -->
                            <div class="owl-item">
                                <div class="special_item d-flex flex-column align-items-center justify-content-center">
                                    <div class="special_item_background"><img src="images/special_3.jpg" alt="https://unsplash.com/@hellolightbulb"></div>
                                    <div class="special_item_content text-center">
                                        <div class="special_category">Visite</div>
                                        <div class="special_title"><a href="offers.php">Brehat</a></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Special Offers Item -->
                            <div class="owl-item">
                                <div class="special_item d-flex flex-column align-items-center justify-content-center">
                                    <div class="special_item_background"><img src="images/special_4.jpg" alt="https://unsplash.com/@dnevozhai"></div>
                                    <div class="special_item_content text-center">
                                        <div class="special_category">Visite</div>
                                        <div class="special_title"><a href="offers.php">Groix</a></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Special Offers Item -->
                            <div class="owl-item">
                                <div class="special_item">
                                    <div class="special_item_background"><img src="images/special_5.jpg" alt="https://unsplash.com/@garciasaldana_"></div>
                                    <div class="special_item_content text-center">
                                        <div class="special_category">Bain de soleil</div>
                                        <div class="special_title"><a href="offers.php">Batz sur mer</a></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Special Offers Item -->
                            <div class="owl-item">
                                <div class="special_item d-flex flex-column align-items-center justify-content-center">
                                    <div class="special_item_background"><img src="images/special_6.jpg" alt="https://unsplash.com/@dnevozhai"></div>
                                    <div class="special_item_content text-center">
                                        <div class="special_category">Visite</div>
                                        <div class="special_title"><a href="offers.php">Molene</a></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Special Offers Item -->
                            <div class="owl-item">
                                <div class="special_item d-flex flex-column align-items-center justify-content-center">
                                    <div class="special_item_background"><img src="images/special_7.jpg" alt="https://unsplash.com/@dnevozhai"></div>
                                    <div class="special_item_content text-center">
                                        <div class="special_category">Visiting</div>
                                        <div class="special_title"><a href="offers.php">Ouessant</a></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Special Offers Item -->
                            <div class="owl-item">
                                <div class="special_item d-flex flex-column align-items-center justify-content-center">
                                    <div class="special_item_background"><img src="images/special_8.jpg" alt="https://unsplash.com/@dnevozhai"></div>
                                    <div class="special_item_content text-center">
                                        <div class="special_category">Visite</div>
                                        <div class="special_title"><a href="offers.php">Ile_de_Sein</a></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Special Offers Item -->
                            <div class="owl-item">
                                <div class="special_item d-flex flex-column align-items-center justify-content-center">
                                    <div class="special_item_background"><img src="images/special_9.jpg" alt="https://unsplash.com/@dnevozhai"></div>
                                    <div class="special_item_content text-center">
                                        <div class="special_category">Visite</div>
                                        <div class="special_title"><a href="offers.php">Houat</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="special_slider_nav d-flex flex-column align-items-center justify-content-center">
                            <img src="images/special_slider.png" alt="">
                        </div>
                    </div>
                </div>
            </div>


            <!-- Video -->
            <div class="video_section d-flex flex-column align-items-center justify-content-center">
                <!-- Image by https://unsplash.com/@peecho -->
                <div class="video_background parallax-window" data-parallax="scroll" data-image-src="images/video.jpg" data-speed="0.8"></div>
                <div class="video_content">
                    <div class="video_title"><strong>Notre offre du jour</strong></div>
                    <div class="video_subtitle"><strong>Belle-île-en-Mer</strong></div>
                    <div class="video_play">
                        <a  class="video" href="video/Belle-ile-en-mer-video.mp4">
                            <svg version="1.1" id="Layer_1" class="play_button" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 width="140px" height="140px" viewBox="0 0 140 140" enable-background="new 0 0 140 140" xml:space="preserve">
                                <g id="Layer_2">
                                    <circle class="play_circle" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-miterlimit="10" cx="70.333" cy="69.58" r="68.542"/>
                                    <polygon class="play_triangle" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-miterlimit="10" points="61.583,56 61.583,84.417 84.75,70 	"/>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>

            <!-- Newsletter -->
            <?php include("php/newletter.inc.php"); ?>

            <!-- Footer -->

            <?php include("php/footer.inc.php"); ?>
        </div>

        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="styles/bootstrap4/popper.js"></script>
        <script src="styles/bootstrap4/bootstrap.min.js"></script>
        <script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
        <script src="plugins/easing/easing.js"></script>
        <script src="plugins/parallax-js-master/parallax.min.js"></script>
        <script src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
        <script src="js/custom.js"></script>
    </body>
</html>