<!DOCTYPE html>
<html lang="en">
<head>
<title>Offers</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Destino project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/offers_styles.css">
<link rel="stylesheet" type="text/css" href="styles/offers_responsive.css">
</head>
<body>

<div class="super_container">
	
	<?php include("php/navbar.inc.php"); ?>
	
	<!-- Home -->

	<div class="home">
		<!-- Image by https://unsplash.com/@peecho -->
		<div class="home_background parallax-window" data-parallax="scroll" data-image-src="images/offers.jpg" data-speed="0.8"></div>
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_content_inner">
							<div class="home_title">Offres</div>
							<div class="home_breadcrumbs">
								<ul class="home_breadcrumbs_list">
									<li class="home_breadcrumb"><a href="index.php">Accueil</a></li>
									<li class="home_breadcrumb">Offres</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>

	<!-- Find Form -->

	<div class="find">
		<!-- Image by https://unsplash.com/@garciasaldana_ -->
		<div class="find_background_container prlx_parent">
			<div class="find_background prlx" style="background-image:url(images/find.jpg)"></div>
		</div>
		<!-- <div class="find_background parallax-window" data-parallax="scroll" data-image-src="images/find.jpg" data-speed="0.8"></div> -->
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="find_title text-center">Trouvez votre voyage</div>
				</div>
				<div class="col-12">
					<div class="find_form_container">
						<form action="Choix_horaire.php" method="post" id="find_form" class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap">
							<div class="find_item">
                                        <div>Liaisons :</div>
                                        <select name="liaison" id="adventure" class="dropdown_item_select find_input">
                                            <!-- form_find -->
                                            <?php include"php/formFind.inc.php";
                                            entreeLiaison();
                                            ?>
                                        </select>
                                    </div>
                                    <div class="find_item">
                                        <div>Date :</div>
                                        <input type="date" name="date" id="adventure" class="dropdown_item_select find_input">
                                    </div>
                            
							<button class="button find_button">Envoyer</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

    
	<!-- Offers -->

	<div class="offers">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h2>Top destinations</h2>
						<div>Les meilleures destinations pour vous</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col">
					<div class="items item_grid clearfix">

						<!-- Item -->
						<div class="item clearfix rating_5">
							<div class="item_image"><img src="images/top_1.jpg" alt=""></div>
							<div class="item_content">
								<div class="item_title">Aix en provence</div>
								
								<div class="rating rating_5" data-rating="5">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
								</div>
								<div class="item_text">Aix-en-Provence est une ville universitaire de la région Provence-Alpes-Côte d'Azur, dans le Sud de la France. C’est là qu’est né le peintre Paul Cézanne, figure du postimpressionnisme. Une promenade relie différents endroits importants de la vie de Cézanne, dont la maison où il a grandi, le Jas de Bouffan, et son atelier. La Sainte-Victoire, montagne de calcaire surplombant la ville et son paysage alentour, fut le sujet de nombre de ses œuvres.</div>
							</div>
						</div>

						<!-- Item -->
						<div class="item clearfix rating_3">
							<div class="item_image"><img src="images/top_2.jpg" alt=""></div>
							<div class="item_content">
								<div class="item_title">Belle-île-en-mer</div>
								
								<div class="rating rating_5" data-rating="3">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
								</div>
								<div class="item_text">Au sud du Morbihan, à 15km de Quiberon, Belle-île-en-Mer est la seconde des grandes îles de la façade atlantique, après Oléron. Son climat doux et ensoleillé en fait une destination très prisée des amateurs de sites naturels et de faune et flore préservées, de randonnée pédestre et cycliste, de sports nautiques et du bien-être. Plusieurs hôtels, campings, chambres d'hôtes, locations, résidences et villages de vacances vous y accueillent toute l'année, pour de longs ou courts séjours, évènements professionnels ou personnels.</div>
								
							</div>
						</div>

						<!-- Item -->
						<div class="item clearfix rating_4">
							<div class="item_image"><img src="images/top_3.jpg" alt=""></div>
							<div class="item_content">
								<div class="item_title">Ile de Groix</div>
								
								<div class="rating rating_4" data-rating="4">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
								</div>
								<div class="item_text">Groix [gʁwa] est une île et une commune bretonne du département du Morbihan. Elle se trouve dans le golfe de Gascogne, au large de la côte sud de la Bretagne, au nord-ouest de Belle-Île-en-Mer et en face de Ploemeur. Elle constitue une commune, et, jusqu'à 2015, constituait aussi le canton de Groix.</div>
								
							</div>
						</div>

						<!-- Item -->
						<div class="item clearfix rating_4">
							<div class="item_image"><img src="images/top_4.jpg" alt=""></div>
							<div class="item_content">
								<div class="item_title">Batz</div>
								
								<div class="rating rating_4" data-rating="5">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
								</div>
								<div class="item_text">L'île de Batz est une petite commune insulaire située au large de Roscoff dans le nord Finistère. Après seulement un quart d'heure de traversée en bateau, l'île s'offre à vous avec un dépaysement complet.

                                Si l'île de Batz est souvent décrite comme "discrète", elle saura néanmoins vous charmer par la richesse de sa faune, de sa flore, de son patrimoine et par la chaleur de ses habitants.</div>
								
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Newsletter -->
        <?php include("php/newletter.inc.php"); ?>
	
    <!-- Footer -->
	    <?php include("php/footer.inc.php"); ?>
</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/Isotope/isotope.pkgd.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/offers_custom.js"></script>
</body>
</html>