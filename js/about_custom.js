/* JS Document */

/******************************

[Table of Contents]

1. Vars and Inits
2. Set Header
3. Initialize Hamburger
4. Init Parallax
5. Initialize Milestones
6. Init SVG
7. Init Search


******************************/


function Print() {
    window.print();
}

$(document).ready(function()
{

    $(document).ready(function () {
		
		
			var quantitiy=0;
		   $('.quantity-right-plus1').click(function(e){
				
				
				// Stop acting like a button
				e.preventDefault();
				// Get the field name
				var quantity = parseInt($('#quantity1').val());
				
				// If is not undefined
					
					$('#quantity1').val(quantity + 1);
					// Increment
			});

			 $('.quantity-left-minus1').click(function(e){
				// Stop acting like a button
				e.preventDefault();
				// Get the field name
				var quantity = parseInt($('#quantity1').val());
				
				// If is not undefined
			  
					// Increment
					if(quantity>0){
					$('#quantity1').val(quantity - 1);
					}
			});
			
			
			
			var quantitiy=0;
		   $('.quantity-right-plus2').click(function(e){
				
				
				// Stop acting like a button
				e.preventDefault();
				// Get the field name
				var quantity = parseInt($('#quantity2').val());
				
				// If is not undefined
					
					$('#quantity2').val(quantity + 1);
					// Increment
			});

			 $('.quantity-left-minus2').click(function(e){
				// Stop acting like a button
				e.preventDefault();
				// Get the field name
				var quantity = parseInt($('#quantity2').val());
				
				// If is not undefined
			  
					// Increment
					if(quantity>0){
					$('#quantity2').val(quantity - 1);
					}
			});
			
			
			var quantitiy=0;
		   $('.quantity-right-plus3').click(function(e){
				
				
				// Stop acting like a button
				e.preventDefault();
				// Get the field name
				var quantity = parseInt($('#quantity3').val());
				
				// If is not undefined
					
					$('#quantity3').val(quantity + 1);
					// Increment
			});

			 $('.quantity-left-minus3').click(function(e){
				// Stop acting like a button
				e.preventDefault();
				// Get the field name
				var quantity = parseInt($('#quantity3').val());
				
				// If is not undefined
			  
					// Increment
					if(quantity>0){
					$('#quantity3').val(quantity - 1);
					}
			});
			
			var quantitiy=0;
		   $('.quantity-right-plus4').click(function(e){
				
				
				// Stop acting like a button
				e.preventDefault();
				// Get the field name
				var quantity = parseInt($('#quantity4').val());
				
				// If is not undefined
					
					$('#quantity4').val(quantity + 1);
					// Increment
			});

			 $('.quantity-left-minus4').click(function(e){
				// Stop acting like a button
				e.preventDefault();
				// Get the field name
				var quantity = parseInt($('#quantity4').val());
				
				// If is not undefined
			  
					// Increment
					if(quantity>0){
					$('#quantity4').val(quantity - 1);
					}
			});
			
			var quantitiy=0;
		   $('.quantity-right-plus5').click(function(e){
				
				
				// Stop acting like a button
				e.preventDefault();
				// Get the field name
				var quantity = parseInt($('#quantity5').val());
				
				// If is not undefined
					
					$('#quantity5').val(quantity + 1);
					// Increment
			});

			 $('.quantity-left-minus5').click(function(e){
				// Stop acting like a button
				e.preventDefault();
				// Get the field name
				var quantity = parseInt($('#quantity5').val());
				
				// If is not undefined
			  
					// Increment
					if(quantity>0){
					$('#quantity5').val(quantity - 1);
					}
			});
			
			var quantitiy=0;
		   $('.quantity-right-plus6').click(function(e){
				
				
				// Stop acting like a button
				e.preventDefault();
				// Get the field name
				var quantity = parseInt($('#quantity6').val());
				
				// If is not undefined
					
					$('#quantity6').val(quantity + 1);
					// Increment
			});

			 $('.quantity-left-minus6').click(function(e){
				// Stop acting like a button
				e.preventDefault();
				// Get the field name
				var quantity = parseInt($('#quantity5').val());
				
				// If is not undefined
			  
					// Increment
					if(quantity>0){
					$('#quantity6').val(quantity - 1);
					}
			});
			
			
			var quantitiy=0;
		   $('.quantity-right-plus7').click(function(e){
				
				
				// Stop acting like a button
				e.preventDefault();
				// Get the field name
				var quantity = parseInt($('#quantity7').val());
				
				// If is not undefined
					
					$('#quantity7').val(quantity + 1);
					// Increment
			});

			 $('.quantity-left-minus7').click(function(e){
				// Stop acting like a button
				e.preventDefault();
				// Get the field name
				var quantity = parseInt($('#quantity7').val());
				
				// If is not undefined
			  
					// Increment
					if(quantity>0){
					$('#quantity7').val(quantity - 1);
					}
			});
			
			var quantitiy=0;
		   $('.quantity-right-plus8').click(function(e){
				
				
				// Stop acting like a button
				e.preventDefault();
				// Get the field name
				var quantity = parseInt($('#quantity8').val());
				
				// If is not undefined
					
					$('#quantity8').val(quantity + 1);
					// Increment
			});

			 $('.quantity-left-minus8').click(function(e){
				// Stop acting like a button
				e.preventDefault();
				// Get the field name
				var quantity = parseInt($('#quantity8').val());
				
				// If is not undefined
			  
					// Increment
					if(quantity>0){
					$('#quantity8').val(quantity - 1);
					}
			});
			
        
        
        //AJOUTE LA CLASS SELECTED AU CLICK
            $('tr').on('click', function () {
            
              $("#checkBox").prop('checked' , true);
              $( "tr" ).filter( ".selected" ).toggleClass('selected');
              $(this).toggleClass('selected');
                $(".selected td").appendChild(doc.createElement('</form">'));
                $()
            });

        
        
        
        
        
            $('.btn-filter').on('click', function () {
              var $target = $(this).data('target');
              if ($target != 'all') {
                $('.table tr').css('display', 'none');
                $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
              } else {
                $('.table tr').css('display', 'none').fadeIn('slow');
              }
            });

         });
    
	"use strict";

	/* 

	1. Vars and Inits

	*/

	var header = $('.header');
	var hamb = $('.hamburger');
	var hambActive = false;
	var menuActive = false;
	var ctrl = new ScrollMagic.Controller();

	setHeader();

	$(window).on('resize', function()
	{
		setHeader();
	});

	$(document).on('scroll', function()
	{
		setHeader();
	});

	initHamburger();
	initParallax();
	initMilestones();
	initSvg();
	initSearch();

	/* 

	2. Set Header

	*/

	function setHeader()
	{
		if(window.innerWidth < 992)
		{
			if($(window).scrollTop() > 100)
			{
				header.addClass('scrolled');
			}
			else
			{
				header.removeClass('scrolled');
			}
		}
		else
		{
			if($(window).scrollTop() > 100)
			{
				header.addClass('scrolled');
			}
			else
			{
				header.removeClass('scrolled');
			}
		}
		// if(window.innerWidth > 991 && menuActive)
		// {
		// 	closeMenu();
		// }
	}

	/* 

	3. Initialize Hamburger

	*/

	function initHamburger()
	{
		if($('.hamburger').length)
		{
			hamb.on('click', function(event)
			{
				event.stopPropagation();

				if(!menuActive)
				{
					openMenu();
					
					$(document).one('click', function cls(e)
					{
						if($(e.target).hasClass('menu_mm'))
						{
							$(document).one('click', cls);
						}
						else
						{
							closeMenu();
						}
					});
				}
				else
				{
					$('.menu_container').removeClass('active');
					menuActive = false;
				}
			});
		}
	}

	function openMenu()
	{
		var fs = $('.menu_container');
		fs.addClass('active');
		hambActive = true;
		menuActive = true;
	}

	function closeMenu()
	{
		var fs = $('.menu_container');
		fs.removeClass('active');
		hambActive = false;
		menuActive = false;
	}

	/* 

	4. Init Parallax

	*/

	function initParallax()
	{
		// Add parallax effect to every element with class prlx
		// Add class prlx_parent to the parent of the element
		if($('.prlx_parent').length && $('.prlx').length)
		{
			var elements = $('.prlx_parent');

			elements.each(function()
			{
				var ele = this;
				var bcg = $(ele).find('.prlx');

				var slideParallaxScene = new ScrollMagic.Scene({
			        triggerElement: ele,
			        triggerHook: 1,
			        duration: "200%"
			    })
			    .setTween(TweenMax.from(bcg, 1, {y: '-30%', ease:Power0.easeNone}))
			    .addTo(ctrl);
			});
		}
	}

	/* 

	5. Initialize Milestones

	*/

	function initMilestones()
	{
		if($('.milestone_counter').length)
		{
			var milestoneItems = $('.milestone_counter');

	    	milestoneItems.each(function(i)
	    	{
	    		var ele = $(this);
	    		var endValue = ele.data('end-value');
	    		var eleValue = ele.text();

	    		/* Use data-sign-before and data-sign-after to add signs
	    		infront or behind the counter number */
	    		var signBefore = "";
	    		var signAfter = "";

	    		if(ele.attr('data-sign-before'))
	    		{
	    			signBefore = ele.attr('data-sign-before');
	    		}

	    		if(ele.attr('data-sign-after'))
	    		{
	    			signAfter = ele.attr('data-sign-after');
	    		}

	    		var milestoneScene = new ScrollMagic.Scene({
		    		triggerElement: this,
		    		triggerHook: 'onEnter',
		    		reverse:false
		    	})
		    	.on('start', function()
		    	{
		    		var counter = {value:eleValue};
		    		var counterTween = TweenMax.to(counter, 4,
		    		{
		    			value: endValue,
		    			roundProps:"value", 
						ease: Circ.easeOut, 
						onUpdate:function()
						{
							document.getElementsByClassName('milestone_counter')[i].innerHTML = signBefore + counter.value + signAfter;
						}
		    		});
		    	})
			    .addTo(ctrl);
	    	});
		}
	}

	/* 

	6. Init SVG

	*/

	function initSvg()
	{
		jQuery('img.svg').each(function()
		{
			var $img = jQuery(this);
			var imgID = $img.attr('id');
			var imgClass = $img.attr('class');
			var imgURL = $img.attr('src');

			jQuery.get(imgURL, function(data)
			{
				// Get the SVG tag, ignore the rest
				var $svg = jQuery(data).find('svg');

				// Add replaced image's ID to the new SVG
				if(typeof imgID !== 'undefined') {
				$svg = $svg.attr('id', imgID);
				}
				// Add replaced image's classes to the new SVG
				if(typeof imgClass !== 'undefined') {
				$svg = $svg.attr('class', imgClass+' replaced-svg');
				}

				// Remove any invalid XML tags as per http://validator.w3.org
				$svg = $svg.removeAttr('xmlns:a');

				// Replace image with new SVG
				$img.replaceWith($svg);
			}, 'xml');
		});
	}

	/* 

	7. Init Search

	*/

	function initSearch()
	{
		if($('.search').length)
		{
			var search = $('.search');
			search.on('click', function(e)
			{
				var target = $(e.target);
				if(!target.hasClass('ctrl_class'))
				{
					$(this).toggleClass('active');
				}
			});
		}
	}
});