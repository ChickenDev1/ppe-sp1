<!DOCTYPE html>
<html lang="en">
    <head>
        <title>MarieTeam</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Destino project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/horaire_styles.css">
<link rel="stylesheet" type="text/css" href="styles/horaire_responsive.css">

    </head>
    <body>
        <?php
         if( !isset($_POST['liaison']) || !isset($_POST['date']) ){
                    header('Location: index.php');
                }
        
        require('php/horaireTable_traitement.php');
        require("php/formFind.inc.php");
        ?>
        
        
        <div class="super_container">

            <?php include("php/navbar.inc.php");?>
            
            <!-- Home -->

            <div class="home">
                <div class="home_background" style="background-image:url(images/home.jpg)"></div>
                <div class="home_content">
                    <div class="home_content_inner">
                        <div class="home_text_large">Voyage</div>
                        <div class="home_text_small">Partir plus loin </div>
                    </div>
                </div>
            </div>

            <!-- Find Form -->

            <div class="find">
                
                
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="find_title text-center">Trouvez votre voyage</div>
                        </div>
                        <div class="col-12">
                            
                            <div class="find_form_container">
                               
                                <form action="Choix_horaire.php" method="post" id="find_form" class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap">
                                    <div class="find_item">
                                        <div>Liaisons :</div>
                                        <select name="liaison" id="adventure" class="dropdown_item_select find_input">
                                            <!-- form_find -->
                                            <?php
                                                entreeLiaison();
                                            ?>
                                        </select>
                                    </div>
                                    <div class="find_item">
                                        <div>Date :</div>
                                        <input type="date" name="date" id="adventure" class="dropdown_item_select find_input">
                                    </div>

                                    <button type="submit" class="button find_button">Envoyer</button>
                                </form>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>


           <?php 
                
                    $liaison = explode(" - ", $_POST['liaison']);
                    $port_depart = $liaison[0];
                    $port_arriver = $liaison[1];
                    
                    $date =  $_POST['date'];
                    remplissage_tableau_horaire($date, $port_depart, $port_arriver);
                
           
                include("php/footer.inc.php"); 
            ?>
        </div>
    
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="styles/bootstrap4/popper.js"></script>
        <script src="styles/bootstrap4/bootstrap.min.js"></script>
        <script src="plugins/greensock/TweenMax.min.js"></script>
        <script src="plugins/greensock/TimelineMax.min.js"></script>
        <script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
        <script src="plugins/greensock/animation.gsap.min.js"></script>
        <script src="plugins/greensock/ScrollToPlugin.min.js"></script>
        <script src="plugins/easing/easing.js"></script>
        <script src="plugins/parallax-js-master/parallax.min.js"></script>
        <script src="js/about_custom.js"></script>
    </body>
</html>