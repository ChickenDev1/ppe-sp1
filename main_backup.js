    function get_connection_3_month(){

       try
        {
            // On se connecte à MySQL: base de donnée marieteam
            $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', ''); //Connexion à la BD
        }
        catch(Exception $e)
        {
            // En cas d'erreur, on affiche un message et on arrête tout
            die('Erreur : '.$e->getMessage());
        }
        
        $str_date_today = date("y.m.d");
       
        //recuperation du mois en cour
        $mois_en_cours = new DateTime($str_date_today);
        $mois_en_cours = $mois_en_cours->format('m');
        
        //recuperation d'un mois avant le mois en cour
        $month1 = new DateTime($str_date_today);
        $month1->sub(new DateInterval('P1M')); //Où 'P12M' indique 'Période de 12 Mois'
        $month1 = $month1->format('m');
        
        //recuperation de deux mois avant le mois en cour
        $month2 = new DateTime($str_date_today);
        $month2->sub(new DateInterval('P2M')); //Où 'P12M' indique 'Période de 12 Mois'
        $month2 = $month2->format('m');
        
        //recuperation de deux mois avant le mois en cour
        $month3 = new DateTime($str_date_today);
        $month3->sub(new DateInterval('P3M')); //Où 'P12M' indique 'Période de 12 Mois'
        $month3 = $month2->format('m');
        
        //recuperation de deux mois avant le mois en cour
        $month4 = new DateTime($str_date_today);
        $month4->sub(new DateInterval('P4M')); //Où 'P12M' indique 'Période de 12 Mois'
        $month4 = $month2->format('m');
        
        //recuperation de deux mois avant le mois en cour
        $month5 = new DateTime($str_date_today);
        $month5->sub(new DateInterval('P4M')); //Où 'P12M' indique 'Période de 12 Mois'
        $month5 = $month2->format('m');
        
         $req_mois_en_cour = $bdd->query('SELECT COUNT(ID_CONNEXION) AS nbr_mois_en_cour
                                    FROM connexion
                                    WHERE DATE LIKE "%'.$mois_en_cours.'%"'); 
        
        $donnees = $req_mois_en_cour->fetch(); //on analyse le type de retour
        
        $i_nbr_mois_en_cour = $donnees['nbr_mois_en_cour']; //on stocke les données de retour dans la variable i_nbr_connexion   
        
        //REQUETE MOIS 1
        //on compte le nombre de connexion de tous les utilisateurs dans la table utilisateur 
        $req_month1 = $bdd->query('SELECT COUNT(ID_CONNEXION) AS nbr_connexion_month1
                                    FROM connexion
                                    WHERE DATE LIKE "%'.$month1.'%"'); 
        
        $donnees = $req_month1->fetch(); //on analyse le type de retour
        
        $i_nbr_connexion_month1 = $donnees['nbr_connexion_month1']; //on stocke les données de retour dans la variable i_nbr_connexion      
        
        
        //REQUETE MOIS 2
        $req_month2 = $bdd->query('SELECT COUNT(ID_CONNEXION) AS nbr_connexion_month2
                                    FROM connexion
                                    WHERE DATE LIKE "%'.$month2.'%"'); 
        
        $donnees = $req_month2->fetch(); //on analyse le type de retour
        
        $i_nbr_connexion_month2 = $donnees['nbr_connexion_month2']; //on stocke les données de retour dans la variable i_nbr_connexion   
        
        
        //REQUETE MOIS 3
        $req_month3 = $bdd->query('SELECT COUNT(ID_CONNEXION) AS nbr_connexion_month3
                                    FROM connexion
                                    WHERE DATE LIKE "%'.$month3.'%"'); 
        
        $donnees = $req_month3->fetch(); //on analyse le type de retour
        
        $i_nbr_connexion_month3 = $donnees['nbr_connexion_month3']; //on stocke les données de retour dans la variable i_nbr_connexion   
        
        $array = array(10,30,10,20,15,50);
        return $array;                            
    }