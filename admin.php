<?php
session_start();
if(!empty($_SESSION['is_logged_in']) && $_SESSION['is_logged_in'] == 1) {

} else {
    header("Location: connexion.php"); 
} 
?>
<!-DOCTYPE html->
<html lang="en">

    <head>
        <!-- Required meta tags-->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="au theme template">
        <meta name="author" content="Hau Nguyen">
        <meta name="keywords" content="au theme template">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

        <!-- Title Page-->
        <title>Dashboard</title>

        <!-- Fontfaces CSS-->
        <link href="css/font-face.css" rel="stylesheet" media="all">
        <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
        <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
        <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

        <!-- Bootstrap CSS-->
        <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

        <!-- Vendor CSS-->
        <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
        <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
        <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
        <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
        <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
        <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
        <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

        <!-- Main CSS-->
        <link href="styles/theme.css" rel="stylesheet" media="all">
        <link rel="stylesheet" type="text/css" href="styles/admin_styles.css">



    </head>

    <body class="animsition">

        <?php

        require("php/admin_traitement.php");

        ?>

        <div class="page-wrapper">


            <nav class="navbar1 navbar-expand-lg navbar-dark bg-dark navbar-inverse">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <span class="navbar-text">
                        Administrateur
                    </span>
                    <ul class="nav navbar-nav ml-auto">
                        <li><a href="index.php">Accueil</a></li>
                    </ul>
                </div>
            </nav>


            <!-- PAGE CONTAINER-->
            <div class="page container">
                <!-- ACCORDIONS TABS-->
                <div class="accordions_tabs">
                    <div class="container">

                        <div class="row accordions_tabs_container">
                            <div class="col-lg-12">
								
                                <!-- ACCORDION -->

                                <div class="accordions_content">
                                    <div class="accordion_container">
                                        <div class="accordion d-flex flex-row align-items-center"><div>Ajout d'une liaison</div></div>
                                        <div class="accordion_panel">

                                            <form  action="#" method="post" id="find_form" class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap">
                                                <div class="find_item">
                                                     <div>Depart :</div>
												<select name="depart" id="depart" class="find_input">
												<!-- ajout liaison -->
												<?php include"php/add_liaison.php";
											    ajoutLiaison();
												?>
												</select>
                                                </div>
												<div class="find_item">
                                                <div>Arrivée :</div>
												<select name="arrivee" id="arrivee" class="find_input">
												<!-- ajout liaison -->
												<?php 
												ajoutLiaison();
												?>	
												</select>
                                                </div>
                                                <div class="find_item">
                                                <div>Secteur :</div>
                                                <select name="secteur" id="secteur" class="find_input">
												<!-- ajout secteur -->
												<?php 
												ajoutSecteur();
												?>	
												</select>
                                                </div>
                                                <div class="find_item">
                                                    <div>Distance :</div>
                                                    <input min="0" type="number" name="distance" id="distance" class="find_input">
                                                </div>
                                                <div class="find_item">
                                                    <button name="btnValider" type="submit" class="button find_button ">Ajout</button>
                                                </div>
                                            </form>
											<?php
									if(isset($_POST['btnValider'])){
										insert_into_liaison($_POST['distance'], $_POST['secteur'], $_POST['depart'], $_POST['arrivee']);
										echo "<div><center><font color='red'>Liaison ajoutée avec succés</font></center></div>";
									} 
								?>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row m-t-25">
                    <div class="container">
                        <form action="" method="post" id="period_form">
                            <select name="periode" class=" period_input ">
                                <?php 
                                    entreePeriode();
                                ?>
                            </select>
                            <button type="submit" class="period_button">Envoyer</button>
                        </form>
                    </div>
                </div>
                
                <div class="row m-t-25">
                    <div class="col-sm-6 col-lg-6">
                        <div class="overview-item overview-item--c2">
                            <div class="overview__inner">
                                <div class="overview-box clearfix">
                                    <div class="icon">
                                        <i class="fas fa-user"></i>
                                    </div>
                                    <div class="text">
                                        <h2>
                                            <?php
                                                if(isset($_POST['periode'])){
                                                    echo ChiffreAffaire($_POST['periode']);
                                                }else{
                                                     echo 0;
                                                }
                                                
                                            ?>

                                        </h2>
                                        <span>Chiffre d'Affaire</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="overview-item overview-item--c3">
                            <div class="overview__inner">
                                <div class="overview-box clearfix">
                                    <div class="icon">
                                        <i class="fas fa-car"></i>
                                    </div>
                                    <div class="text">
                                        <h2>
                                            <?php
                                            
                                                if(isset($_POST['periode'])){
                                                    echo somme_catA($_POST['periode']);
                                                }else{
                                                     echo 0;
                                                }
                                            ?>
                                        </h2>
                                        <span>Nombre de passager transportés</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row m-t-25">
                    <div class="col-sm-6 col-lg-6">
                        <div class="overview-item overview-item--c2">
                            <div class="overview__inner">
                                <div class="overview-box clearfix">
                                    <div class="icon">
                                        <i class="fas fa-truck"></i>
                                    </div>
                                    <div class="text">
                                        <h2>
                                            <?php
                                              if(isset($_POST['periode'])){
                                                    echo somme_catB($_POST['periode']);
                                                }else{
                                                     echo 0;
                                                }
                                            
                                            ?>

                                        </h2>
                                        <span>Nombre total de reservation pour les vehicules de &lt;2m</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="overview-item overview-item--c3">
                            <div class="overview__inner">
                                <div class="overview-box clearfix">
                                    <div class="icon">
                                        <i class="fas fa-ship"></i>
                                    </div>
                                    <div class="text">
                                        <h2>
                                            <?php
                                            
                                                if(isset($_POST['periode'])){
                                                    echo somme_catC($_POST['periode']);
                                                }else{
                                                     echo 0;
                                                }
                                            

                                            ?>
                                        </h2>
                                        <span>Nombre total de reservation pour les vehicules de &gt;2m</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-12">
                        <div class="overview-item overview-item--c3">
                            <div class="overview__inner">
                                <div class="overview-box clearfix">
                                    <div class="icon">
                                        <i class="fas fa-ship"></i>
                                    </div>
                                    <div class="text">
                                        <h2>
                                            <?php
                                            
                                                if(isset($_POST['periode'])){
                                                         echo somme_catA($_POST['periode'])+somme_catB($_POST['periode'])+somme_catC($_POST['periode']);
                                                }else{
                                                     echo 0;
                                                }
                                            ?>
                                        </h2>
                                        <span>Nombre Total de passager et vehicule transportés</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
                </div>
                <!-- Footer -->

                    <footer class="footer">
                        <div class="container">
                            <div class="row">
                                <!-- Footer Column -->
                                <div class="col-lg-4 footer_col">
                                    <div class="footer_about">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </footer>

            </div>


        <!-- Jquery JS-->
        <script src="vendor/jquery-3.2.1.min.js"></script>
        <!-- Bootstrap JS-->
        <script src="vendor/bootstrap-4.1/popper.min.js"></script>
        <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
        <!-- Vendor JS       -->
        <script src="vendor/slick/slick.min.js">
        </script>
        <script src="vendor/wow/wow.min.js"></script>
        <script src="vendor/animsition/animsition.min.js"></script>
        <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
        </script>
        <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
        <script src="vendor/counter-up/jquery.counterup.min.js">
        </script>
        <script src="vendor/circle-progress/circle-progress.min.js"></script>
        <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
        <script src="vendor/chartjs/Chart.bundle.min.js"></script>
        <script src="vendor/select2/select2.min.js">
        </script>

        <!-- Main JS-->
        <script src="js/main.js"></script>

        <!-- Elements JS-->
        <script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
        <script src="js/elements_custom.js"></script>	


    </body>

</html>
<!-- end document-->
