<!DOCTYPE html>
<html lang="en">
<head>
<title>About us</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Destino project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/reservation_styles.css">
<link rel="stylesheet" type="text/css" href="styles/reservation_responsive.css">
    
<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/about_custom.js"></script>
</head>
<body>

<div class="super_container">
	
	<!-- Header -->

	<header class="header">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center justify-content-start">

						<!-- Logo -->
						<div class="logo_container">
							<div class="logo">
								<div>MarieTeam</div>
								<div>Agence de voyage</div>
								<div class="logo_image"><img src="images/logo.png" alt=""></div>
							</div>
						</div>

						<!-- Main Navigation -->
						<nav class="main_nav ml-auto">
							<ul class="main_nav_list">
								<li class="main_nav_item"><a href="index.php">Accueil</a></li>
								<li class="main_nav_item active"><a href="#">A propos</a></li>
								<li class="main_nav_item"><a href="offers.php">Offres</a></li>
								<li class="main_nav_item"><a href="contact.php">Contact</a></li>
							</ul>
						</nav>

						<!-- Search -->
						<div class="search">
							<form action="#" class="search_form">
								<input type="search" name="search_input" class="search_input ctrl_class" required="required" placeholder="Keyword">
								<button type="submit" class="search_button ml-auto ctrl_class"><img src="images/search.png" alt=""></button>
							</form>
						</div>

						<!-- Hamburger -->
						<div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>

					</div>
				</div>
			</div>
		</div>
	</header>

    
	<!-- Menu -->

	<div class="menu_container menu_mm">

		<!-- Menu Close Button -->
		<div class="menu_close_container">
			<div class="menu_close"></div>
		</div>

		<!-- Menu Items -->
		<div class="menu_inner menu_mm">
			<div class="menu menu_mm">
				<div class="menu_search_form_container">
					<form action="#" id="menu_search_form">
						<input type="search" class="menu_search_input menu_mm">
						<button id="menu_search_submit" class="menu_search_submit" type="submit"><img src="images/search_2.png" alt=""></button>
					</form>
				</div>
				<ul class="menu_list menu_mm">
					<li class="menu_item menu_mm"><a href="index.php">Accueil</a></li>
					<li class="menu_item menu_mm"><a href="#">A propos</a></li>
					<li class="menu_item menu_mm"><a href="offers.php">Offres</a></li>
					<li class="menu_item menu_mm"><a href="contact.php">Contact</a></li>
				</ul>

				<!-- Menu Social -->
				
				<div class="menu_social_container menu_mm">
					<ul class="menu_social menu_mm">
						<li class="menu_social_item menu_mm"><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
						<li class="menu_social_item menu_mm"><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						<li class="menu_social_item menu_mm"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li class="menu_social_item menu_mm"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li class="menu_social_item menu_mm"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					</ul>
				</div>

				<div class="menu_copyright menu_mm">Colorlib All rights reserved</div>
			</div>

		</div>

	</div>
	
	<!-- Home -->

	<div class="home">
		<!-- Image by https://unsplash.com/@peecho -->
		<div class="home_background parallax-window" data-parallax="scroll" data-image-src="images/about_background.jpg" data-speed="0.8"></div>
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_content_inner">
							<div class="home_title">A propos</div>
							<div class="home_breadcrumbs">
								<ul class="home_breadcrumbs_list">
									<li class="home_breadcrumb"><a href="index.php">Accueil</a></li>
									<li class="home_breadcrumb">A propos</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>

	<!-- Find Form -->

	<div class="find">
		<!-- Image by https://unsplash.com/@garciasaldana_ -->
		<div class="find_background_container prlx_parent">
			<div class="find_background prlx" style="background-image:url(images/find.jpg)"></div>
		</div>
		<!-- <div class="find_background parallax-window" data-parallax="scroll" data-image-src="images/find.jpg" data-speed="0.8"></div> -->
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="find_title text-center">Trouvez votre voyage</div>
				</div>
				
				<form action="#" id="find_form" class="d-flex flex-md-row flex-wrap">
				
					<div class="col-12">
						<div class="find_form_container">
							<div   class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap">
								<div class="find_item">
									<div>Nom :</div>
									<input type="text" name="nom" id="adventure" class="find_input">
								</div>
								
								<div class="find_item">
									<div>Adresse :</div>
									<input type="text" name="prenom" id="adventure" class="find_input">
								</div>
								<div class="find_item">
									<div>Code postal : </div>
									<input type="text" name="cp" id="adventure" class="find_input">
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-12">
						<div class="find_form_container">
							<div   class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap">
								
								<div class="find_item">
									<div>Adulte : </div>
									<div class="input-group ">
										<span class="input-group-btn">
											<button type="button" class="quantity-left-minus1 btn btn-number"  data-type="minus" data-field="">
											  -
											</button>
										</span>
										<input type="text" id="quantity1" name="Adulte" class="find_input form-control input-number" value="0" min="0" max="10">
										<span class="input-group-btn">
											<button type="button" class="quantity-right-plus1 btn btn-number" data-type="plus" data-field="">
												+
											</button>
										</span>
									</div>
									
								</div>
								
								<div class="find_item">
									<div>Junior 8 - 18 ans : </div>
									<div class="input-group">
										<span class="input-group-btn">
											<button type="button" class="quantity-left-minus2 btn btn-number"  data-type="minus" data-field="">
											  -
											</button>
										</span>
										<input type="text" id="quantity2" name="junior" class="find_input form-control input-number" value="0" min="0" max="10">
										<span class="input-group-btn">
											<button type="button" class="quantity-right-plus2 btn btn-number" data-type="plus" data-field="">
												+
											</button>
										</span>
									</div>
								</div>
								
								<div class="find_item">
									<div>Junior 0 - 7 ans : </div>
									<div class="input-group">
										<span class="input-group-btn">
											<button type="button" class="quantity-left-minus3 btn btn-number"  data-type="minus" data-field="">
											  -
											</button>
										</span>
										<input type="text" id="quantity3" name="junior" class="find_input form-control input-number" value="0" min="0" max="10">
										<span class="input-group-btn">
											<button type="button" class="quantity-right-plus3 btn btn-number" data-type="plus" data-field="">
												+
											</button>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="find_form_container">
							<div   class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap">
								
								<div class="find_item">
									<div>Voiture &lt; 4 mètres : </div>
									<div class="input-group ">
										<span class="input-group-btn">
											<button type="button" class="quantity-left-minus4 btn btn-number"  data-type="minus" data-field="">
											  -
											</button>
										</span>
										<input type="text" id="quantity4" name="Adulte" class="find_input form-control input-number" value="0" min="0" max="10">
										<span class="input-group-btn">
											<button type="button" class="quantity-right-plus4 btn btn-number" data-type="plus" data-field="">
												+
											</button>
										</span>
									</div>
									
								</div>
								
								<div class="find_item">
									<div>Voiture &lt; 5 mètres : </div>
									<div class="input-group">
										<span class="input-group-btn">
											<button type="button" class="quantity-left-minus5 btn btn-number"  data-type="minus" data-field="">
											  -
											</button>
										</span>
										<input type="text" id="quantity5" name="junior" class="find_input form-control input-number" value="0" min="0" max="10">
										<span class="input-group-btn">
											<button type="button" class="quantity-right-plus5 btn btn-number" data-type="plus" data-field="">
												+
											</button>
										</span>
									</div>
								</div>
								
								<div class="find_item">
									<div>Fourgon : </div>
									<div class="input-group">
										<span class="input-group-btn">
											<button type="button" class="quantity-left-minus6 btn btn-number"  data-type="minus" data-field="">
											  -
											</button>
										</span>
										<input type="text" id="quantity6" name="junior" class="find_input form-control input-number" value="0" min="0" max="10">
										<span class="input-group-btn">
											<button type="button" class="quantity-right-plus6 btn btn-number" data-type="plus" data-field="">
												+
											</button>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-12">
						<div class="find_form_container">
							<div   class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap">
								
								<div class="find_item">
									<div>Camping-Car :</div>
									<div class="input-group ">
										<span class="input-group-btn">
											<button type="button" class="quantity-left-minus7 btn btn-number"  data-type="minus" data-field="">
											  -
											</button>
										</span>
										<input type="text" id="quantity7" name="Adulte" class="find_input form-control input-number" value="0" min="0" max="10">
										<span class="input-group-btn">
											<button type="button" class="quantity-right-plus7 btn btn-number" data-type="plus" data-field="">
												+
											</button>
										</span>
									</div>
									
								</div>
								
								<div class="find_item">
									<div>Camion : </div>
									<div class="input-group">
										<span class="input-group-btn">
											<button type="button" class="quantity-left-minus8 btn btn-number"  data-type="minus" data-field="">
											  -
											</button>
										</span>
										<input type="text" id="quantity8" name="junior" class="find_input form-control input-number" value="0" min="0" max="10">
										<span class="input-group-btn">
											<button type="button" class="quantity-right-plus8 btn btn-number" data-type="plus" data-field="">
												+
											</button>
										</span>
									</div>
								</div>
								
								<div class="find_item ">
									<button class="button find_button2">Réserver</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>



    <script>
      
       
    
    </script>
	<!-- Footer -->

	<footer class="footer">
		<div class="container">
			<div class="row">

				<!-- Footer Column -->
				<div class="col-lg-4 footer_col">
					<div class="footer_about">
						<!-- Logo -->
						<div class="logo_container">
							<div class="logo">
								<div>MarieTeam</div>
								<div>Agence de voyage</div>
								<div class="logo_image"><img src="images/logo.png" alt=""></div>
							</div>
						</div>
						<div class="footer_about_text">Nos conseillers formés régulièrement sur l’éventail des destinations que nous proposons, serons ravis de vous accueillir et de vous accompagner dans la préparation de vos vacances.

                        Vous bénéficierez du meilleur rapport qualité prix parmi notre large gamme de séjours, billets des bateaux.

                        A très bientôt dans votre agence MarieTeam !</div>
						<div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
					</div>
				</div>
				<!-- Footer Column -->
				<div class="col-lg-4 footer_col">
					<div class="tags footer_tags">
						<div class="footer_title">Tags</div>
						<ul class="tags_content d-flex flex-row flex-wrap align-items-start justify-content-start">
							<li class="tag"><a href="#">travel</a></li>
							<li class="tag"><a href="#">summer</a></li>
							<li class="tag"><a href="#">cruise</a></li>
							<li class="tag"><a href="#">beach</a></li>
							<li class="tag"><a href="#">offer</a></li>
							<li class="tag"><a href="#">vacation</a></li>
							<li class="tag"><a href="#">trip</a></li>
							<li class="tag"><a href="#">city break</a></li>
							<li class="tag"><a href="#">adventure</a></li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</footer>
</div>

</body>
</html>