<?php
session_start();

	try{
		
		$bdd = new PDO('mysql:host=127.0.0.1;dbname=marieteam', 'root', '');
	}
	catch(exception $e){
		echo 'une erreur est surevenue'.$e;
	}

	if(isset($_POST['formconnexion'])) {
			
		$usernameconnect = $_POST['usernameconnect'];
		$mdpconnect = $_POST['mdpconnect'];

		if(!empty($usernameconnect) AND !empty($mdpconnect)) {

			$req = $bdd->query("SELECT USERNAME,PASSWORD FROM utilisateur WHERE USERNAME = '".$usernameconnect."'AND PASSWORD ='".$mdpconnect."'");
			$req->execute(array($usernameconnect, $mdpconnect));
			$donnees = $req-> rowCount();
			$donnees = $req->fetch();
				// on affiche les informations de l'enregistrement en cours 

				if( $usernameconnect == $donnees['USERNAME']&& $mdpconnect == $donnees['PASSWORD'] ) {
					$_SESSION['is_logged_in'] = 1;
					header("Location: admin.php"); 
				}
				else {
				$_SESSION['is_logged_in'] = 0;
				$erreur = "Mauvais nom ou mot de passe.";
				}
		}
		else {
		$_SESSION['is_logged_in'] = 0;
		$erreur = "Tous les champs doivent être complétés!";
		} 
	}
	else{
	$_SESSION['is_logged_in'] = 0;





	}    

?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Connexion </title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Destino project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/connexion_styles.css">
<link rel="stylesheet" type="text/css" href="styles/connexion_responsive.css">
   
<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/about_custom.js"></script>
</head>
<body>

<div class="super_container">
	
	<!-- Header -->

	<header class="header">
		<div class="container">
		<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center justify-content-start">

						<!-- Logo -->
						<div class="logo_container">
							<div class="logo">
								<div>MarieTeam</div>
								<div>agence de voyage</div>-								
								<div class="logo_image"><img src="images/logo.png" alt=""></div>
							</div>
						</div>

						<!-- Main Navigation -->
						<nav class="main_nav ml-auto">
							<ul class="main_nav_list">
								<li class="main_nav_item"><a href="index.php">Accueil</a></li>
								<li class="main_nav_item"><a href="offers.php">Offres</a></li>
								<li class="main_nav_item"><a href="horaire.php">Horaires</a></li>
								<li class="main_nav_item"><a href="contact.php">Contact</a></li>
								<li class="main_nav_item active"><a href="#">Connexion</a></li>
							</ul>
						</nav>

						<!-- Search -->
						<div class="search">
							<form action="#" class="search_form">
								<input type="search" name="search_input" class="search_input ctrl_class" required="required" placeholder="Keyword">
								<button type="submit" class="search_button ml-auto ctrl_class"><img src="images/search.png" alt=""></button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

    
	<!-- Menu -->

	<div class="menu_container menu_mm">

		<!-- Menu Close Button -->
		<div class="menu_close_container">
			<div class="menu_close"></div>
		</div>

	</div>
	
	<!-- Home -->
	<div class="home">
		<!-- Image by https://unsplash.com/@peecho -->
		<div class="home_background parallax-window" data-parallax="scroll" data-image-src="images/about_background.jpg" data-speed="0.8"></div>
		<div class="container">
			<div class="row">				
			<div class="col">
					<div class="home_content">
						<div class="home_content_inner">
							<div class="home_title">Page de connexion</div>
							<div class="home_breadcrumbs">
								<ul class="home_breadcrumbs_list">
									<li class="home_breadcrumb"><a href="index.html">Accueil</a></li>
									<li class="home_breadcrumb">Connexion</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>

	<!-- Find Form -->
	

	<div class="find">
		<!-- Image by https://unsplash.com/@garciasaldana_ -->
		<div class="find_background_container prlx_parent">
			<div class="find_background prlx" style="background-image:url(images/find.jpg)"></div>
		</div>
		<div class="find_background parallax-window" data-parallax="scroll" data-image-src="images/find.jpg" data-speed="0.8"></div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="find_title text-center">Connectez-vous</div>

				</div>
				<div class="col-12">
					<div class="find_form_container">
						<form method="POST" action="#" id="find_form"  class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap">
							<div class="find_item" style="margin:auto auto auto auto">
								<div class="row" >
									<label>Nom de compte :</label>
									<input type="text" name="usernameconnect" placeholder="Nom d'utilisateur" class="find_input" />
									<label>Mot de passe:</label>
									<input type="password" name="mdpconnect" placeholder="Mot de passe" class="find_input" />
									<input type="submit" name="formconnexion" value="Se connecter !" class="button find_button " />
								</div>
								<br>
								<h1 style="text-align:center;font-size:medium;">
								<?php
								if(isset($erreur)) {
								echo '<font color="red">'.$erreur."</font>";
								}
								?>
								</h1>
							</div>
						</form>					
					</div>           
				</div>
			</div>
		</div>
	</div>
	<!-- Footer -->

	<footer class="footer">
		<div class="container">
			<div class="row">

				<!-- Footer Column -->
				<div class="col-lg-4 footer_col">
					<div class="footer_about">
						<!-- Logo -->
						<div class="logo_container">
							<div class="logo">
								<div>MarieTeam</div>
								<div>agence de voyage</div>
								<div class="logo_image"><img src="images/logo.png" alt=""></div>
							</div>
						</div>
						<div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							Copyright &copy;<script>document.write(new Date().getFullYear());</script>
						</div>
					</div>
				</div>

			</div>
		</div>
	</footer>
</div>

</body>
</html>