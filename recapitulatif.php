<!DOCTYPE html>
<html lang="en">
    <head>
        <title>About us</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="Destino project">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
        <link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="styles/reservation_styles.css">
        <link rel="stylesheet" type="text/css" href="styles/reservation_responsive.css">

        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="styles/bootstrap4/popper.js"></script>
        <script src="styles/bootstrap4/bootstrap.min.js"></script>
        <script src="plugins/greensock/TweenMax.min.js"></script>
        <script src="plugins/greensock/TimelineMax.min.js"></script>
        <script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
        <script src="plugins/greensock/animation.gsap.min.js"></script>
        <script src="plugins/greensock/ScrollToPlugin.min.js"></script>
        <script src="plugins/easing/easing.js"></script>
        <script src="plugins/parallax-js-master/parallax.min.js"></script>
        <script src="js/about_custom.js"></script>
    </head>
    <body>

        <div class="super_container">

            <?php 
            session_start();

            include("php/navbar.inc.php"); 

            require("php/recapitulatif_traitement.php");
            require("php/reservation_traitement.php");

            $adulte = $_POST['adulte'];
            $junior = $_POST['junior'];
            $enfant = $_POST['baby'];
            $inf4 = $_POST['inf4'];
            $sup5 = $_POST['sup5'];
            $forgon = $_POST['fourgon'];
            $campingCar = $_POST['campingCar'];
            $camion = $_POST['camion'];

            $catA = $adulte + $junior + $enfant;
            $catB = $inf4 + $sup5;
            $catC = $forgon + $campingCar + $camion;
            
            $prixTotal = prix($_SESSION['numtrav'], $_POST['adulte'], $_POST['junior'], $_POST['baby'], $_POST['inf4'], $_POST['sup5'], $_POST['fourgon'], $_POST['camion'], $_POST['campingCar']); 
            
            $nom = $_POST['nom'];
            $prenom = $_POST['prenom'];
            $mail = $_POST['mail'];
            $cp = $_POST['cp'];
            $ville = $_POST['ville'];
            $adresse = $_POST['adresse'];

            $id_client = insert_into_client($nom, $prenom, $mail, $adresse, $ville, $cp);

            insert_into_reservation($id_client, $_SESSION['numtrav'], $catA, $catB, $catC, $prixTotal);

            ?>

            <!-- Home -->

            <div class="home">
                <!-- Image by https://unsplash.com/@peecho -->
                <div class="home_background parallax-window" data-parallax="scroll" data-image-src="images/about_background.jpg" data-speed="0.8"></div>
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="home_content">
                                <div class="home_content_inner">
                                    <div class="home_title">A propos</div>
                                    <div class="home_breadcrumbs">
                                        <ul class="home_breadcrumbs_list">
                                            <li class="home_breadcrumb"><a href="index.php">Accueil</a></li>
                                            <li class="home_breadcrumb">A propos</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>		
            </div>

            <!-- Find Form -->

            <div class="find">
                <!-- Image by https://unsplash.com/@garciasaldana_ -->
                <div class="find_background_container prlx_parent">
                    <div class="find_background prlx" style="background-image:url(images/find.jpg)"></div>
                </div>
                <!-- <div class="find_background parallax-window" data-parallax="scroll" data-image-src="images/find.jpg" data-speed="0.8"></div> -->
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="find_title text-center">Trouvez votre voyage</div>
                        </div>
                        <div class="col-12">
                            <div class="find_form_container">
                                <div   class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap">

                                    <div class="find_item">
                                        <p>Liaison: <?php echo get_nom_port_liaison($_SESSION['numtrav']); ?></p>
                                    </div>

                                    <div class="find_item">
                                        <p>Bateau: <?php echo get_nom_bateau($_SESSION['numtrav']); ?></p>
                                    </div>
                                    <div class="find_item">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-12">
                            <div class="find_form_container">
                                <div   class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap">

                                    <div class="find_item">
                                        <p>Nom: <?php echo $_POST['nom']; ?> </p>
                                    </div>

                                    <div class="find_item">
                                        <p>Prenom: <?php echo $_POST['adresse']; ?> </p>
                                    </div>

                                    <div class="find_item">
                                        <p>Code Postale: <?php echo $_POST['cp']; ?> </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-12">
                            <div class="find_form_container">
                                <div   class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap">

                                    <div class="find_item">
                                        <p>Nombre d'adulte: <?php echo $_POST['adulte']; ?> </p>
                                    </div>

                                    <div class="find_item">
                                        <p>Junior 8 - 18 ans : <?php echo $_POST['junior']; ?> </p>
                                    </div>

                                    <div class="find_item">
                                        <p>Junior 0 - 7 ans : <?php echo $_POST['baby']; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="find_form_container">
                                <div   class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap">

                                    <div class="find_item">
                                        <p>Voiture &lt; 4 mètres : <?php echo $_POST['inf4']; ?> </p>
                                    </div>

                                    <div class="find_item">
                                        <p>Voiture &lt; 5 mètres : <?php echo $_POST['sup5']; ?> </p>
                                    </div>

                                    <div class="find_item">
                                        <p>Fourgon : <?php echo $_POST['fourgon']; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="find_form_container">
                                <div   class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap">

                                    <div class="find_item">
                                        <p>Camion: <?php echo $_POST['camion']; ?> </p>
                                    </div>

                                    <div class="find_item">
                                        <p>Camping-Car: <?php echo $_POST['campingCar']; ?> </p>
                                    </div>

                                    <div class="find_item">
                                        <p>Total : <?php totalApresReduction($id_client)?></p>
                                    </div>
                                </div>
                                <div class="find_item ">
                                    <button onclick="print()" class="button find_button2">Imprimer</button>
                                </div>
                            </div>
                        </div>
                    </div>	
                </div>
            </div>
        </div>

    </body>
</html>