<!DOCTYPE html>
<html lang="en">
<head>
<title>About us</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Destino project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/reservation_styles.css">
<link rel="stylesheet" type="text/css" href="styles/reservation_responsive.css">
    
<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/about_custom.js"></script>
</head>
<body>

<div class="super_container">
	    <?php
			if( !isset($_POST['num_traversee'])){
				header('Location: index.php');
			}
			session_start();
			
			include("php/navbar.inc.php");
            require("php/reservation_traitement.php");
			$numtrav = $_POST['num_traversee'];
		
			$_SESSION['numtrav'] = $numtrav;
    ?>
	<!-- Home -->

	<div class="home">
		<!-- Image by https://unsplash.com/@peecho -->
		<div class="home_background parallax-window" data-parallax="scroll" data-image-src="images/about_background.jpg" data-speed="0.8"></div>
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_content_inner">
							<div class="home_title">A propos</div>
							<div class="home_breadcrumbs">
								<ul class="home_breadcrumbs_list">
									<li class="home_breadcrumb"><a href="index.php">Accueil</a></li>
									<li class="home_breadcrumb">A propos</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>

	<!-- Find Form -->

	<div class="find">
		<!-- Image by https://unsplash.com/@garciasaldana_ -->
		<div class="find_background_container prlx_parent">
			<div class="find_background prlx" style="background-image:url(images/find.jpg)"></div>
		</div>
		<!-- <div class="find_background parallax-window" data-parallax="scroll" data-image-src="images/find.jpg" data-speed="0.8"></div> -->
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="find_title text-center">Trouvez votre voyage</div>
				</div>
				
				<form action="recapitulatif.php" method="post" id="find_form" class="d-flex flex-md-row flex-wrap">
				
					<div class="col-12">
						<div class="find_form_container">
							<div   class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap" >
								<div class="find_item">
									<div>Nom : </div>
									<input type="text" name="nom" id="adventure" class="find_input">
								</div>
								
                                <div class="find_item">
									<div>Prenom : </div>
									<input type="text" name="prenom" id="adventure" class="find_input">
								</div>
                                
								<div class="find_item">
									<div>Adresse mail:</div>
									<input type="mail" name="mail" id="adventure" class="find_input">
								</div>
                                
							</div>
						</div>
					</div>
					
                    <div class="col-12">
						<div class="find_form_container">
							<div   class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap" >
                                 <div class="find_item">
									<div>Adresse: </div>
									<input type="text" name="adresse" id="adventure" class="find_input">
								</div>
								<div class="find_item">
									<div>Ville : </div>
									<input type="text" name="ville" id="adventure" class="find_input">
								</div>
                                
                                <div class="find_item">
									<div>Code postal : </div>
									<input type="text" name="cp" id="adventure" class="find_input">
								</div>
                               
                               
							</div>
						</div>
					</div>
                
					<div class="col-12">
						<div class="find_form_container">
							<div   class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap">
								
								<div class="find_item">
									<div>Adulte : <?php echo tarif($numtrav, 1);?> € </div>
									<div class="input-group ">
										<span class="input-group-btn">
											<button type="button" class="quantity-left-minus1 btn btn-number"  data-type="minus" data-field="">
											  -
											</button>
										</span>
										<input type="text" id="quantity1" name="adulte" class="find_input form-control input-number" value="0" min="0" max="10" >
										<span class="input-group-btn">
											<button type="button" class="quantity-right-plus1 btn btn-number" data-type="plus" data-field="">
												+
											</button>
										</span>
									</div>
									
								</div>
								
								<div class="find_item">
									<div>Junior 8 - 18 ans : <?php echo tarif($numtrav, 2);?> €</div>
									<div class="input-group">
										<span class="input-group-btn">
											<button type="button" class="quantity-left-minus2 btn btn-number"  data-type="minus" data-field="">
											  -
											</button>
										</span>
										<input type="text" id="quantity2" name="junior" class="find_input form-control input-number" value="0" min="0" max="10">
										<span class="input-group-btn">
											<button type="button" class="quantity-right-plus2 btn btn-number" data-type="plus" data-field="">
												+
											</button>
										</span>
									</div>
								</div>
								
								<div class="find_item">
									<div>Junior 0 - 7 ans : <?php echo tarif($numtrav, 3);?> €</div>
									<div class="input-group">
										<span class="input-group-btn">
											<button type="button" class="quantity-left-minus3 btn btn-number"  data-type="minus" data-field="">
											  -
											</button>
										</span>
										<input type="text" id="quantity3" name="baby" class="find_input form-control input-number" value="0" min="0" max="10">
										<span class="input-group-btn">
											<button type="button" class="quantity-right-plus3 btn btn-number" data-type="plus" data-field="">
												+
											</button>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="find_form_container">
							<div   class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap">
								
								<div class="find_item">
									<div>Voiture &lt; 4 mètres : <?php echo tarif($numtrav, 4);?> €</div>
									<div class="input-group ">
										<span class="input-group-btn">
											<button type="button" class="quantity-left-minus4 btn btn-number"  data-type="minus" data-field="">
											  -
											</button>
										</span>
										<input type="text" id="quantity4" name="inf4" class="find_input form-control input-number" value="0" min="0" max="10">
										<span class="input-group-btn">
											<button type="button" class="quantity-right-plus4 btn btn-number" data-type="plus" data-field="">
												+
											</button>
										</span>
									</div>
									
								</div>
								
								<div class="find_item">
									<div>Voiture &lt; 5 mètres : <?php echo tarif($numtrav,5);?> €</div>
									<div class="input-group">
										<span class="input-group-btn">
											<button type="button" class="quantity-left-minus5 btn btn-number"  data-type="minus" data-field="">
											  -
											</button>
										</span>
										<input type="text" id="quantity5" name="sup5" class="find_input form-control input-number" value="0" min="0" max="10">
										<span class="input-group-btn">
											<button type="button" class="quantity-right-plus5 btn btn-number" data-type="plus" data-field="">
												+
											</button>
										</span>
									</div>
								</div>
								
								<div class="find_item">
									<div>Fourgon : <?php echo tarif($numtrav, 6);?> €</div>
									<div class="input-group">
										<span class="input-group-btn">
											<button type="button" class="quantity-left-minus6 btn btn-number"  data-type="minus" data-field="">
											  -
											</button>
										</span>
										<input type="text" id="quantity6" name="fourgon" class="find_input form-control input-number" value="0" min="0" max="10">
										<span class="input-group-btn">
											<button type="button" class="quantity-right-plus6 btn btn-number" data-type="plus" data-field="">
												+
											</button>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-12">
						<div class="find_form_container">
							<div   class="d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-md-between justify-content-start flex-wrap">
								
								<div class="find_item">
									<div>Camping-Car : <?php echo tarif($numtrav, 7);?> €</div>
									<div class="input-group ">
										<span class="input-group-btn">
											<button type="button" class="quantity-left-minus7 btn btn-number"  data-type="minus" data-field="">
											  -
											</button>
										</span>
										<input type="text" id="quantity7" name="campingCar" class="find_input form-control input-number" value="0" min="0" max="10">
										<span class="input-group-btn">
											<button type="button" class="quantity-right-plus7 btn btn-number" data-type="plus" data-field="">
												+
											</button>
										</span>
									</div>
									
								</div>
								
								<div class="find_item">
									<div>Camion : <?php echo tarif($numtrav, 8);?> €</div>
									<div class="input-group">
										<span class="input-group-btn">
											<button type="button" class="quantity-left-minus8 btn btn-number"  data-type="minus" data-field="">
											  -
											</button>
										</span>
										<input type="text" id="quantity8" name="camion" class="find_input form-control input-number" value="0" min="0" max="10">
										<span class="input-group-btn">
											<button type="button" class="quantity-right-plus8 btn btn-number" data-type="plus" data-field="">
												+
											</button>
										</span>
									</div>
								</div>
								
								<div class="find_item ">
									<button type="submit" class="button find_button2">Réserver</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

    
    
   
    
    
	<!-- Footer -->

	    <?php include("php/footer.inc.php"); ?>
</div>

</body>
</html>