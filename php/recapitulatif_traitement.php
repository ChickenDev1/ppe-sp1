<?php



function get_nom_port_liaison($num_traversee){
        ///////////////////////////// BASE DE DONNEE
        try{

            // On se connecte à MySQL: base de donnée marieteam
            $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', ''); //Connexion à la BD
        }
        catch(Exception $e){

            // En cas d'erreur, on affiche un message et on arrête tout
            die('la connexion a la base de donnée a échoué<br>Erreur : '.$e->getMessage());
        }
       

      ///////////////////////////// SELECTION NOM DU PORT DE DEPART

      try{
        $req_nom_port_depart = $bdd->query('SELECT NOM_PORT AS nom_port_depart
                                            FROM port
                                            INNER JOIN liaison ON liaison.ID_DEPART = port.ID_PORT
                                            INNER JOIN traversee ON liaison.CODE_LIAISON = traversee.CODE_LIAISON
                                            WHERE traversee.NUM_TRAVERSEE = '.$num_traversee);
      }
        catch(Exception $e){

            die("la requête de récupération de l'id depart a échoué<br>Erreur : ".$e->getMessage());
        }


        $donnees_req_nom_port_depart= $req_nom_port_depart->fetch(); //On stock les données de la requête

        $str_nom_port_depart = $donnees_req_nom_port_depart['nom_port_depart']; // On stocke les données de retour dans la variable $str_id_port_depart
    

      

    ///////////////////////////// SELECTION NOM DU PORTS ARRIVER

    try{
        $req_nom_port_arrivee = $bdd->query('SELECT NOM_PORT AS nom_port_arrivee
                                            FROM port
                                            INNER JOIN liaison ON liaison.ID_ARRIVEE = port.ID_PORT
                                            INNER JOIN traversee ON liaison.CODE_LIAISON = traversee.CODE_LIAISON
                                            WHERE traversee.NUM_TRAVERSEE ='.$num_traversee);
        }
        catch(Exception $e){

            die("la requête de récupération de l'id depart a échoué<br>Erreur : ".$e->getMessage());
        }


        $donnees_req_nom_port_arrivee= $req_nom_port_arrivee->fetch(); //On stock les données de la requête

        $str_nom_port_arrivee = $donnees_req_nom_port_arrivee['nom_port_arrivee']; // On stocke les données de retour dans la variable $str_id_port_depart
    
    return $str_nom_port_depart.' à '.$str_nom_port_arrivee; 
}


///////////////////REQUETE GET NOM BATEAU



function get_nom_bateau($num_traversee){
    
    ///////////////////////////// BASE DE DONNEE
        try{

            // On se connecte à MySQL: base de donnée marieteam
            $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', ''); //Connexion à la BD
        }
        catch(Exception $e){

            // En cas d'erreur, on affiche un message et on arrête tout
            die('la connexion a la base de donnée a échoué<br>Erreur : '.$e->getMessage());
        }
    
    

    
     ///////////////////////////// SELECTION NOM BATEAU

        try{
        $req_nom_bateau= $bdd->query('SELECT NOM_BATEAU AS nom_bateau
                                      FROM bateau
                                      INNER JOIN traversee ON bateau.ID_BATEAU = traversee.ID_BATEAU
                                      WHERE NUM_TRAVERSEE = '.$num_traversee);
        }
        catch(Exception $e){

            die("la requête de récupération de l'id depart a échoué<br>Erreur : ".$e->getMessage());
        }


        $donnees_req_nom_bateau = $req_nom_bateau->fetch(); //On stock les données de la requête

        $str_nom_bateau = $donnees_req_nom_bateau['nom_bateau']; // On stocke les données de retour dans la variable $str_id_port_depart
    
    return $str_nom_bateau;
}



///////////////////REQUETE ADD CLIENT

function insert_into_client($nomClient, $prenomClient, $mailClient, $adresseClient, $villeClients, $cpClient){
    
        ///////////////////////////// BASE DE DONNEE
        try{

            // On se connecte à MySQL: base de donnée marieteam
            $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', ''); //Connexion à la BD
        }
        catch(Exception $e){

            // En cas d'erreur, on affiche un message et on arrête tout
            die('la connexion a la base de donnée a échoué<br>Erreur : '.$e->getMessage());
        }
    
        ///////////////////////////// ADD CLIENTS IF

        try{
        $req_add_client= $bdd->query('SELECT COUNT(ID_CLIENT) as correspondance
                                      FROM client
                                      WHERE MAIL ="'.$mailClient.'"'
                                    );
        }
        catch(Exception $e){

            die("la requête de récupération de l'id depart a échoué<br>Erreur : ".$e->getMessage());
        }


        $donnees_req_add_client= $req_add_client->fetch(); //On stock les données de la requête
    
        $i_add_client = $donnees_req_add_client['correspondance']; // On stocke les données de retour dans la variable $str_id_port_depart
    
    
        if($i_add_client == 0){
            try{
                $req_add_reservation = $bdd->query("INSERT INTO `client` (`NOM`, `PRENOM` , `MAIL`, `ADRESSE`, `CP`, `VILLE`) 
                                                    VALUES ('".$nomClient."', '".$prenomClient."', '".$mailClient."', '".$adresseClient."', '".$villeClients."','".$cpClient."')");
            }
            catch(Exception $e){

                die("la requête d'ajout de reservation a échoué<br>Erreur : ".$e->getMessage());
            }
        }
    
         try{
        $req_get_id_client= $bdd->query('SELECT ID_CLIENT AS id_client
                                      FROM client
                                      WHERE MAIL ="'.$mailClient.'"');
        }
    
        catch(Exception $e){

            die("la requête de récupération de l'id depart a échoué<br>Erreur : ".$e->getMessage());
        }


        $donnees_req_get_id_client= $req_get_id_client->fetch(); //On stock les données de la requête
    
        $i_id_client = $donnees_req_get_id_client['id_client']; // On stocke les données de retour dans la variable $str_id_port_depart
    
        return $i_id_client;
}


///////////////////REQUETE ADD RESERVATION


function insert_into_reservation($idclient, $num_traversee, $nbrReservA, $nbrReservB, $nbrReservC, $prixTotal){
    
    
    
      ///////////////////////////// BASE DE DONNEE
        try{

            // On se connecte à MySQL: base de donnée marieteam
            $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', ''); //Connexion à la BD
        }
        catch(Exception $e){

            // En cas d'erreur, on affiche un message et on arrête tout
            die('la connexion a la base de donnée a échoué<br>Erreur : '.$e->getMessage());
        }
    
        try{
        $req_add_reservation = $bdd->query(' INSERT INTO `reservation` (`DATE_RESERVATION`, `ID_CLIENT`, `NUM_TRAVERSEE`, `NBR_RESERV_A`,        `NBR_RESERV_B`, `NBR_RESERV_C`, `PRIX_TOTAL` ) 
                                             VALUES ("'.date('y-m-d').'",'.$idclient.','.$num_traversee.','.$nbrReservA.','.$nbrReservB.','.$nbrReservC.','.$prixTotal.')'
                                          
                                          );
        }
        catch(Exception $e){

            die("la requête d'ajout de reservation a échoué<br>Erreur : ".$e->getMessage());
        }

    
}


function prix ($uneTraversee, $placeAdulte, $placeBebe, $placeJunior, $placePetiteVoiture, $placeGrandeVoiture, $placeFourgon, $placeCampingCar, $placeCamion)
{
    $prixTotale = ( $placeAdulte * tarif($uneTraversee, 1))+($placeBebe * tarif($uneTraversee, 3))+($placeJunior * tarif($uneTraversee, 2))+($placePetiteVoiture * tarif($uneTraversee, 4))+($placeGrandeVoiture * tarif($uneTraversee, 5))+($placeFourgon * tarif($uneTraversee, 6))+($placeCampingCar * tarif($uneTraversee, 7))+($placeCamion * tarif($uneTraversee, 8));
    
    return $prixTotale;
}


function totalApresReduction($id_client)
{
    ///////////////////////////// BASE DE DONNEE
        try{

            // On se connecte à MySQL: base de donnée marieteam
            $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', ''); //Connexion à la BD
        }
        catch(Exception $e){

            // En cas d'erreur, on affiche un message et on arrête tout
            die('la connexion a la base de donnée a échoué<br>Erreur : '.$e->getMessage());
        }
        
        ///////////////////////////// reduction

        try{
        $req_prix = $bdd->query("SELECT PRIX_TOTAL AS prix
                                 FROM reservation
                                 WHERE ID_CLIENT = ".$id_client."
                                 AND DATE_RESERVATION ='".date('y-m-d')."'
                                 ORDER BY `reservation`.`NUM_RESERVATION` DESC");
        }
        catch(Exception $e){

            die("la requête de récupération de l'id depart a échoué<br>Erreur : ".$e->getMessage());
        }
    
    $donnees_req_prix= $req_prix->fetch(); //On stock les données de la requête
    
        $i_prix = $donnees_req_prix['prix']; // On stocke les données de retour dans la variable $str_id_port_depart
    
        echo $i_prix;
}





?>