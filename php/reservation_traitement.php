<?php
    
    
function tarif($uneTraversee,$unType){

    try
    {
        // On se connecte à MySQL: base de donnée marieteam
        $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', ''); //Connexion à la BDD
    }
    catch(Exception $e)
    {
        // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
    }

    //on compte le nombre de reservation dans la table reservation
    $req_tarif = $bdd->query("SELECT tarifer.TARIF AS unTarif
                            FROM tarifer 
                            INNER JOIN periode ON tarifer.ID_PERIODE = periode.ID_PERIODE
                            INNER JOIN liaison on tarifer.CODE_LIAISON = liaison.CODE_LIAISON
                            INNER JOIN traversee on liaison.CODE_LIAISON = liaison.CODE_LIAISON

                            WHERE traversee.NUM_TRAVERSEE = ".$uneTraversee."
                            AND periode.DATE_DEB < traversee.DATE_TRAVERSEE
                            AND periode.DATE_FIN > traversee.DATE_TRAVERSEE
                            AND tarifer.ID_TYPE = ".$unType);

    $donnees = $req_tarif->fetch(); //on analyse le type de retour

    $i_tarif = $donnees['unTarif']; //on stocke les données de retour dans la variable $i_nbr_reservation

    return $i_tarif ; 
    
   
}

?>