
<?php


function entreePeriode(){
    
    try
    {
        // On se connecte à MySQL
        $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', '');
    }
    catch(Exception $e)
    {
        // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
    }

    
    $req_periode = $bdd->query('SELECT DATE_DEB,DATE_FIN
                                FROM periode');

    
    while ($donnees = $req_periode->fetch())
    {
        echo '<option>'.$donnees['DATE_DEB'].' / '.$donnees['DATE_FIN'].'</option>';
    }
    
    
    $req_periode->closeCursor(); // Termine le traitement de la requête
}



function ChiffreAffaire($periode){
    
    $periode = explode(" / ", $periode);
    $dateDebut = $periode[0];
    $dateFin = $periode[1];
    
    
    try
    {
        // On se connecte à MySQL
        $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', '');
    }
    catch(Exception $e)
    {
        // En cas d'erreur, on affiche un message et on arrête tout
        
    }
    
    $req_chiffre_daffaire = $bdd->query('SELECT SUM(PRIX_TOTAL) as chiffreAffaire
                                          FROM reservation
                                          WHERE DATE_RESERVATION > "'.$dateDebut.'"
                                          AND DATE_RESERVATION < "'.$dateFin.'"'); 
    
    $donnees = $req_chiffre_daffaire->fetch();
    
    $i_chiffreAffaire =  $donnees['chiffreAffaire'];
    
    if($i_chiffreAffaire == null){
        $i_chiffreAffaire = 0;
    }
    
    return $i_chiffreAffaire;
}




function nbr_reservation(){

    try
    {
        // On se connecte à MySQL: base de donnée marieteam
        $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', ''); //Connexion à la BDD
    }
    catch(Exception $e)
    {
        // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
    }

    //on compte le nombre de reservation dans la table reservation
    $req_count_reservation = $bdd->query("SELECT COUNT(NUM_RESERVATION) AS nbr_reservation
                                              FROM reservation"); 

    $donnees = $req_count_reservation->fetch(); //on analyse le type de retour

    $i_nbr_connexion = $donnees['nbr_reservation']; //on stocke les données de retour dans la variable $i_nbr_reservation

    echo $i_nbr_connexion ;                                  
}




function nbr_reservation_today(){

    try
    {
        // On se connecte à MySQL: base de donnée marieteam
        $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', ''); //Connexion à la BD
    }
    catch(Exception $e)
    {
        // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
    }

    $str_date_today = date("d.m.y");

    $date = new DateTime($str_date_today);
    $date = $date-> format('m');

    //on aditionnes le nombre de connexion de tous les utilisateurs dans la table utilisateur
    $req = $bdd->query('SELECT COUNT(NUM_RESERVATION) AS nbr_mois_en_cour
                            FROM reservation
                            WHERE DATE_RESERVATION="'.$str_date_today.'"'); 

    $donnees = $req->fetch(); //on analyse le type de retour

    $i_nbr_reservation = $donnees['nbr_mois_en_cour']; //on stocke les données de retour dans la variable i_nbr_connexion

    echo $i_nbr_reservation;                                  
}



function get_reservation_6_month(){

    try
    {
        // On se connecte à MySQL: base de donnée marieteam
        $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', ''); //Connexion à la BD
    }
    catch(Exception $e)
    {
        // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
    }

    $str_date_today = date("y.m.d");

    //recuperation du mois en cour
    $mois_en_cours = new DateTime($str_date_today);
    $mois_en_cours = $mois_en_cours->format('m');

    //recuperation d'un mois avant le mois en cour
    $month1 = new DateTime($str_date_today);
    $month1->sub(new DateInterval('P1M')); //sub indique que l'on enleve et 'P1M' indique 'Période de 1 Mois'
    $month1 = $month1->format('m');

    $month2 = new DateTime($str_date_today);
    $month2->sub(new DateInterval('P2M')); 
    $month2 = $month2->format('m');

    $month3 = new DateTime($str_date_today);
    $month3->sub(new DateInterval('P3M'));
    $month3 = $month3->format('m');

    $month4 = new DateTime($str_date_today);
    $month4->sub(new DateInterval('P4M')); 
    $month4 = $month4->format('m');

    $month5 = new DateTime($str_date_today);
    $month5->sub(new DateInterval('P5M')); 
    $month5 = $month5->format('m');


    //REQUETE MOIS en cours
    //on compte le nombre de connexion de tous les utilisateurs dans la table connexion pour le mois en cours 
    $req_mois_en_cour = $bdd->query("SELECT COUNT(NUM_RESERVATION) AS nbr_mois_en_cour
										FROM reservation
										WHERE DATE_RESERVATION  LIKE '%".$mois_en_cours."%'"); 

    $donnees = $req_mois_en_cour->fetch(); //on stocke le retour de la requete

    $i_nbr_mois_en_cour = $donnees['nbr_mois_en_cour']; //on stocke les données de retour dans la variable i_nbr_connexion   


    //REQUETE MOIS 1
    $req_month1 = $bdd->query("SELECT COUNT(NUM_RESERVATION) AS nbr_reservation_month1
                                    FROM reservation
                                    WHERE DATE_RESERVATION  LIKE '%".$month1."'"); 

    $donnees = $req_month1->fetch(); 

    $i_nbr_reservation_month1 = $donnees['nbr_reservation_month1']; 


    //REQUETE MOIS 2
    $req_month2 = $bdd->query("SELECT COUNT(NUM_RESERVATION) AS nbr_reservation_month2
                                    FROM reservation
                                    WHERE DATE_RESERVATION  LIKE '%".$month2."'"); 

    $donnees = $req_month2->fetch(); 

    $i_nbr_reservation_month2 = $donnees['nbr_reservation_month2']; 


    //REQUETE MOIS 3
    $req_month3 = $bdd->query("SELECT COUNT(NUM_RESERVATION) AS nbr_reservation_month3
                                    FROM reservation
                                    WHERE DATE_RESERVATION  LIKE '%".$month3."'"); 

    $donnees = $req_month3->fetch(); 

    $i_nbr_reservation_month3 = $donnees['nbr_reservation_month3']; 


    //REQUETE MOIS 4
    $req_month4 = $bdd->query("SELECT COUNT(NUM_RESERVATION) AS nbr_reservation_month4
                                    FROM reservation
                                    WHERE DATE_RESERVATION  LIKE '%".$month4."'"); 

    $donnees = $req_month4->fetch(); 

    $i_nbr_reservation_month4 = $donnees['nbr_reservation_month4']; 


    //REQUETE MOIS 5
    $req_month5 = $bdd->query("SELECT COUNT(NUM_RESERVATION) AS nbr_reservation_month5
                                    FROM reservation
                                    WHERE DATE_RESERVATION  LIKE '%".$month5."'"); 

    $donnees = $req_month5->fetch(); 

    $i_nbr_reservation_month5 = $donnees['nbr_reservation_month5']; 

    $array = array($i_nbr_reservation_month5,$i_nbr_reservation_month4,$i_nbr_reservation_month3,$i_nbr_reservation_month2,$i_nbr_reservation_month1,$i_nbr_mois_en_cour);
    return $array;                            
}


function get_name_month(){

    $str_date_today = date("y.m.d");

    //recuperation du mois en cour
    $mois_en_cours = new DateTime($str_date_today);
    $mois_en_cours = $mois_en_cours->format('F.Y');

    //recuperation d'un mois avant le mois en cour
    $month1 = new DateTime($str_date_today);
    $month1->sub(new DateInterval('P1M')); //sub indique que l'on enleve et 'P1M' indique 'Période de 1 Mois'
    $month1 = $month1->format('F.Y');

    $month2 = new DateTime($str_date_today);
    $month2->sub(new DateInterval('P2M')); 
    $month2 = $month2->format('F.Y');

    $month3 = new DateTime($str_date_today);
    $month3->sub(new DateInterval('P3M'));
    $month3 = $month3->format('F.Y');

    $month4 = new DateTime($str_date_today);
    $month4->sub(new DateInterval('P4M')); 
    $month4 = $month4->format('F.Y');

    $month5 = new DateTime($str_date_today);
    $month5->sub(new DateInterval('P5M')); 
    $month5 = $month5->format('F.Y');

    $array2 = array($month5,$month4,$month3,$month2,$month1,$mois_en_cours);

    return $array2;
}

function somme_catA(){

    try
    {
        // On se connecte à MySQL: base de donnée marieteam
        $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', ''); //Connexion à la BD
    }
    catch(Exception $e)
    {
        // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
    }

    $req_sum_catA = $bdd->query('SELECT SUM(NBR_RESERV_A) AS sum_catA
                                     FROM reservation'); 

    $donnees_sum_catA = $req_sum_catA->fetch(); 

    $str_sum_catA = $donnees_sum_catA['sum_catA'];

    if($str_sum_catA == null){
        $str_sum_catA = 0;
    }
    
    
    return $str_sum_catA;                                  
}

function somme_catB(){

    try
    {
        // On se connecte à MySQL: base de donnée marieteam
        $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', ''); //Connexion à la BD
    }
    catch(Exception $e)
    {
        // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
    }

    $req_sum_catB = $bdd->query('SELECT SUM(NBR_RESERV_B) AS sum_catB
                                     FROM reservation'); 

    $donnees_sum_catB = $req_sum_catB->fetch(); 

    $str_sum_catB = $donnees_sum_catB['sum_catB'];

    if($str_sum_catB == null){
        $str_sum_catB = 0;
    }
    
    
    return $str_sum_catB;                                  
}

function somme_catC(){

    try
    {
        // On se connecte à MySQL: base de donnée marieteam
        $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', ''); //Connexion à la BD
    }
    catch(Exception $e)
    {
        // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
    }

    $req_sum_catC = $bdd->query('SELECT SUM(NBR_RESERV_C) AS sum_catC
                                     FROM reservation'); 

    $donnees_sum_catC = $req_sum_catC->fetch(); 

    $str_sum_catC = $donnees_sum_catC['sum_catC'];

    if($str_sum_catC == null){
        $str_sum_catC = 0;
    }
    
    
    return $str_sum_catC;                                  
}
?>