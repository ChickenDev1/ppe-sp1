

	<!-- Footer -->

	<footer class="footer">
		<div class="container">
			<div class="row">

				<!-- Footer Column -->
				<div class="col-lg-4 footer_col">
					<div class="footer_about">
						<!-- Logo -->
						<div class="logo_container">
							<div class="logo">
								<div>MarieTeam</div>
								<div>agence de voyage</div>
								<div class="logo_image"><img src="images/logo.png" alt=""></div>
							</div>
						</div>
						<div class="footer_about_text">
                            Nos conseillers formés régulièrement sur l’éventail des destinations que nous proposons, serons ravis de vous accueillir et de vous accompagner dans la préparation de vos vacances.

                            Vous bénéficierez du meilleur rapport qualité prix parmi notre large gamme de séjours, billets des bateaux.

                            A très bientôt dans votre agence MarieTeam !
                        </div>
						<li class="main_nav_item"><a href="connexion.php">Admin</a></li>
                    </div>
				</div>

			</div>
		</div>
	</footer>
