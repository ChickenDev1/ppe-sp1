
<!-- Header -->

	<header class="header">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center justify-content-start">

						<!-- Logo -->
						<div class="logo_container">
							<div class="logo">
								<div>MarieTeam</div>
								<div>Agence de voyage</div>
								<div class="logo_image"><img src="images/logo.png" alt=""></div>
							</div>
						</div>

						<!-- Main Navigation -->
						<nav class="main_nav ml-auto">
							<ul class="main_nav_list">
								<li class="main_nav_item"><a href="index.php">Accueil</a></li>
								<li class="main_nav_item"><a href="about.php">A propos</a></li>
								<li class="main_nav_item"><a href="offers.php">Offres</a></li>
                                <li class="main_nav_item"><a href="liaison.php">Liaison</a></li>
                                <li class="main_nav_item"><a href="tarif.php">Tarif</a></li>
								<li class="main_nav_item"><a href="contact.php">Contact</a></li>
							</ul>
						</nav>

						
						<!-- Hamburger -->
						<div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>

					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- Menu -->

	<div class="menu_container menu_mm">

		<!-- Menu Close Button -->
		<div class="menu_close_container">
			<div class="menu_close"></div>
		</div>

		<!-- Menu Items -->
		<div class="menu_inner menu_mm">
			<div class="menu menu_mm">
				<ul class="menu_list menu_mm">
					<li class="menu_item menu_mm"><a href="index.php">Accueil</a></li>
					<li class="menu_item menu_mm"><a href="about.php">A propos</a></li>
					<li class="menu_item menu_mm"><a href="offers.php">Offres</a></li>
                    <li class="menu_item menu_mm"><a href="liaison.php">Liaison</a></li>
                    <li class="menu_item menu_mm"><a href="tarif.php">Tarif</a></li>
					<li class="menu_item menu_mm"><a href="contact.php">Contact</a></li>
				</ul>
			
			</div>

		</div>

	</div>