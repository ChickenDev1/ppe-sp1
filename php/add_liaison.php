<?php

function ajoutLiaison()
{
    try
    {
        // On se connecte à MySQL
        $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', '');
    }
    catch(Exception $e)
    {
        // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
    }

    // Si tout va bien, on peut continuer

    // On récupère tout le contenu de la table liaison
    $req_liaison = $bdd->query('SELECT ID_PORT, NOM_PORT FROM port');

    // On affiche chaque entrée une à une
    while ($donnees = $req_liaison->fetch())
    {
       echo '<option value="'.$donnees['ID_PORT'].'">'.$donnees['NOM_PORT'].'</option>';
    }

    $req_liaison->closeCursor(); // Termine le traitement de la requête
}

function ajoutSecteur()
{
    try
    {
        // On se connecte à MySQL
        $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', '');
    }
    catch(Exception $e)
    {
        // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
    }

    // Si tout va bien, on peut continuer

    // On récupère tout le contenu de la table liaison
    $req_secteur = $bdd->query('SELECT ID_SECTEUR, NOM_SECTEUR FROM secteur');

    // On affiche chaque entrée une à une
    while ($donnees = $req_secteur->fetch())
    {
       echo '<option value="'.$donnees['ID_SECTEUR'].'">'.$donnees['NOM_SECTEUR'].'</option>';
    }

    $req_secteur->closeCursor(); // Termine le traitement de la requête
}

function insert_into_liaison($distance, $secteur, $depart, $arrivee){
    
    ///////////////////////////// BASE DE DONNEE
    try{

            // On se connecte à MySQL: base de donnée marieteam
            $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', ''); //Connexion à la BD
        }
    catch(Exception $e){

            // En cas d'erreur, on affiche un message et on arrête tout
            die('la connexion a la base de donnée a échoué<br>Erreur : '.$e->getMessage());
        }
    
        ///////////////////////////// ADD LIAISON

    try{
                $req_add_liaison = $bdd->query("INSERT INTO `liaison` (CODE_LIAISON, DISTANCE_MILES, ID_SECTEUR,ID_DEPART, ID_ARRIVEE) 
VALUES (null, '".$distance."', '".$secteur."', '".$depart."', '".$arrivee."')");}
													
													
    catch(Exception $e){

                die("la requête d'ajout de liaison a échoué<br>Erreur : ".$e->getMessage());
            }
}
    ?>