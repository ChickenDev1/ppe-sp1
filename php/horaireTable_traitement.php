<?php

function remplissage_tableau_horaire($date, $port_dep,$port_arr){

    ///////////////////////////// BASE DE DONNEE
    try{

        // On se connecte à MySQL: base de donnée marieteam
        $bdd = new PDO('mysql:host=localhost;dbname=marieteam;charset=utf8', 'root', ''); //Connexion à la BD
    }
    catch(Exception $e){

        // En cas d'erreur, on affiche un message et on arrête tout
        die('la connexion a la base de donnée a échoué<br>Erreur : '.$e->getMessage());
    }

 
    
    
    ////////////////////////////// SELECTION DES DONNEE DE LA TRAVERSEE 

    try{
        $req_traversee = $bdd->query('SELECT *
                                      FROM traversee
                                      
                                      INNER JOIN liaison on traversee.CODE_LIAISON = liaison.CODE_LIAISON
                                      INNER JOIN port as portD on liaison.ID_DEPART = portD.ID_PORT 
                                      INNER JOIN port as portA on liaison.ID_ARRIVEE = portA.ID_PORT

                                      WHERE portD.NOM_PORT = "'.$port_dep.'"
                                      AND portA.NOM_PORT = "'.$port_arr.'"
                                      AND traversee.DATE_TRAVERSEE ="'.$date.'"');
    }
    catch(Exception $e){

        die("la requête de récupération du num_traversee a échoué<br>Erreur : ".$e->getMessage());
    }
    

?>


    //////////////////////////////////////////////////////////////////////////////////////////////////////////



    <form action="reservation.php" method="post" class="col-md-12 col-md-offset-2 container-fluid">
        <div class="panel panel-default ">
            <div class="panel-body table-container">
                <table class="table table-filter table-border">
                    <thead>
                        <tr>
                            <th></th>
                            <th colspan="2">Traversée</th>
                            <th colspan="3">Place disponible</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Heure</th>
                            <th >Bateau</th>
                            <th >Passager</th>
                            <th>vehicule &lt 2m</th>
                            <th >vehicule &gt 2m</th>
                        </tr>
                    </thead>
                    <tbody>

<?php

                        while ($donnees4= $req_traversee->fetch())
                        {


                            ////////////////////////////// SELECTION NOM BATEAU

                             try{
                            $req_nom_bateau = $bdd->query('SELECT NOM_BATEAU AS nom_bateau
                                                              FROM bateau
                                                              WHERE ID_BATEAU ="'.$donnees4['ID_BATEAU'].'"');
                            }
                            catch(Exception $e){

                                die("la requête de récupération du nom du bateau a échoué<br>Erreur : ".$e->getMessage());
                            }


                            $donnees7= $req_nom_bateau->fetch(); //On stock les données de la requête

                            $str_nom_bateau = $donnees7['nom_bateau']; // On stocke les données de retour dans la variable $str_nom_bateau


                            ////////////////////////////// SELECTION CAPACITE MAX CAT A
                            try{
                                $req_capMax_catA = $bdd->query('SELECT CAPACITE_MAX AS capacite_max
                                                              FROM contenir
                                                              WHERE ID_BATEAU ="'.$donnees4['ID_BATEAU'].'"AND LETTRE = "A"');
                            }
                            catch(Exception $e){

                                die("la requête de récupération du nom du bateau a échoué<br>Erreur : ".$e->getMessage());
                            }


                            $donnees8= $req_capMax_catA->fetch(); //On stock les données de la requête

                            $i_capacite_max = $donnees8['capacite_max']; // On stocke les données de retour dans la variable $str_nom_bateau

                            if($i_capacite_max == null){
                                $i_capacite_max = 0;
                            }
                            ////////////////////////////// COMPTABILISATION NBR DE RESERVATION CAT A

                             try{
                                $req_nbr_reserv_a = $bdd->query('SELECT SUM(NBR_RESERV_A) AS nbr_reserv_a
                                                                FROM Reservation
                                                                WHERE NUM_TRAVERSEE='.$donnees4['NUM_TRAVERSEE']);
                            }
                            catch(Exception $e){

                                die("la requête de récupération du nombre de reservation a échoué<br>Erreur : ".$e->getMessage());
                            }


                            $donnees9= $req_nbr_reserv_a->fetch(); //On stock les données de la requête

                            $nbr_reserv_a = $donnees9['nbr_reserv_a']; // On stocke les données de retour dans la variable $i_capacite_max





                            ////////////////////////////// SELECTION CAPACITE MAX CAT B
                            try{
                                $req_capMax_catB = $bdd->query('SELECT CAPACITE_MAX AS capacite_max
                                                              FROM contenir
                                                              WHERE ID_BATEAU ="'.$donnees4['ID_BATEAU'].'"AND LETTRE = "B"');
                            }
                            catch(Exception $e){

                                die("la requête de récupération du nom du bateau a échoué<br>Erreur : ".$e->getMessage());
                            }


                            $fetch_req_capMax_catB= $req_capMax_catB->fetch(); //On stock les données de la requête

                            $i_capacite_max_catB = $fetch_req_capMax_catB['capacite_max']; // On stocke les données de retour dans la variable $str_nom_bateau


                            ////////////////////////////// COMPTABILISATION NBR DE RESERVATION CAT B

                             try{
                                $req_nbr_reserv_B = $bdd->query('SELECT SUM(NBR_RESERV_B) AS nbr_reserv_b
                                                                FROM Reservation
                                                                WHERE NUM_TRAVERSEE='.$donnees4['NUM_TRAVERSEE']);
                            }
                            catch(Exception $e){

                                die("la requête de récupération du nombre de reservation a échoué<br>Erreur : ".$e->getMessage());
                            }


                            $fetch_req_nbr_reserv_catB= $req_nbr_reserv_B->fetch(); //On stock les données de la requête

                            $nbr_reserv_b = $fetch_req_nbr_reserv_catB['nbr_reserv_b']; // On stocke les données de retour dans la variable $i_capacite_max





                            ////////////////////////////// SELECTION CAPACITE MAX CAT C
                            try{
                                $req_capMax_catC = $bdd->query('SELECT CAPACITE_MAX AS capacite_max
                                                              FROM contenir
                                                              WHERE ID_BATEAU ="'.$donnees4['ID_BATEAU'].'"AND LETTRE = "C"');
                            }
                            catch(Exception $e){

                                die("la requête de récupération du nom du bateau a échoué<br>Erreur : ".$e->getMessage());
                            }


                            $fetch_req_capMax_catC= $req_capMax_catC->fetch(); //On stock les données de la requête

                            $i_capacite_max_catC = $fetch_req_capMax_catC['capacite_max']; // On stocke les données de retour dans la variable $str_nom_bateau


                            ////////////////////////////// COMPTABILISATION NBR DE RESERVATION CAT C

                             try{
                                $req_nbr_reserv_C = $bdd->query('SELECT SUM(NBR_RESERV_C) AS nbr_reserv_c
                                                                FROM Reservation
                                                                WHERE NUM_TRAVERSEE='.$donnees4['NUM_TRAVERSEE']);
                            }
                            catch(Exception $e){

                                die("la requête de récupération du nombre de reservation a échoué<br>Erreur : ".$e->getMessage());
                            }


                            $fetch_req_nbr_reserv_catC= $req_nbr_reserv_C->fetch(); //On stock les données de la requête

                            $nbr_reserv_c = $fetch_req_nbr_reserv_catC['nbr_reserv_c']; // On stocke les données de retour dans la variable $i_capacite_max


                            //////////////////////////// CALCULE DES PLACE RESTANTE PAR CATEGORIES  
                            $place_dispo_a = $i_capacite_max - $nbr_reserv_a;
                            $place_dispo_b = $i_capacite_max_catB - $nbr_reserv_b;
                            $place_dispo_c = $i_capacite_max_catC - $nbr_reserv_c;
?>

                        <!--form_find-->
                        <tr>
                            <td><input type="radio" class="form-check-input" name="num_traversee" id="cbx" value=<?php echo '"'.$donnees4['NUM_TRAVERSEE'].'"' ?>> </td>
                            <td><?php echo $donnees4['HEURE_TRAVERSEE']; ?></td>
                            <td><?php echo $str_nom_bateau; ?></td>
                            <td><?php echo $place_dispo_a; ?></td>
                            <td><?php echo $place_dispo_b; ?></td>
                            <td><?php echo $place_dispo_c; ?></td>
                        </tr>
<?php
                    } 

?>
                    </tbody>
                </table>
            </div>
        </div>

            <button type submit class="button find_button2">Réserver</button>

    </form>
<?php
} 

?>
